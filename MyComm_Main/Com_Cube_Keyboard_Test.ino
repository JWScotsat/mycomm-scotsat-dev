TFT_TextBoxClass CharCountBox;
TFT_TextBoxClass SendBox;
TFT_TextBoxClass SaveBox;
TFT_TextBoxClass CancelBox;
TFT_TextBoxClass ContactBox;
TFT_TextBoxClass SendToBox;

#define debugCharBoxEnable false

void KeypadTest_setup(void) {

	TouchKeyboard.setup(&tft);

	TouchKeyboard.DrawKeypad();
	

	
	//*************************************
	//TFT Main box for text input/display
	TFT_TextBox.setup(&tft);
	TFT_TextBox.setLocation(0,210,42,160);
	TFT_TextBox.ResetBox();
	TFT_TextBox.setFontSize(2);
	TFT_TextBox._cursorBlinkEnable = true;

	//*************************************
	//Contact box for text input/display
	ContactBox.setup(&tft);
	ContactBox.setLocation(55,210,22,40);
	ContactBox.ResetBox();
	ContactBox.setFontSize(1);
	ContactBox._cursorBlinkEnable = true;


	//*************************************
	//Contact box for text input/display
	SendToBox.setup(&tft);
	SendToBox.setLocation(0,60,22,40);
	SendToBox.ResetBox();
	SendToBox.setFontSize(1);
	
	
	//*************************************
	//Debug Box for debug display of char count in main tft box
	CharCountBox.setup(&tft);
	CharCountBox.setLocation(180,240,280,320);
	CharCountBox.setFontSize(1);
	
	//*************************************
	//Send Box for future sending gui element
	SendBox.setup(&tft);
	SendBox.setLocation(0,60,164,190);
	SendBox.ResetBox();
	SendBox.setFontSize(2);
	SendBox.PrintText("Send");
	
	//*************************************
	//Save Box for future saving of messages
	SaveBox.setup(&tft);
	SaveBox.setLocation(65,125,164,190);
	SaveBox.ResetBox();
	SaveBox.setFontSize(2);
	SaveBox.PrintText("Save");
	
	//*************************************
	//Cancel Box - return from keypad program
	CancelBox.setup(&tft);
	CancelBox.setLocation(130,210,164,190);
	CancelBox.ResetBox();
	CancelBox.setFontSize(2);
	CancelBox.PrintText("Cancel");
	
}

int clearScreenCounter = 0;
boolean boxLive = false;
void KeypadTest_loop()
{

	KeypadTest_setup();
	ClearScreenMessageWrite();
	boolean isReleased = true;

	int activeTextBox = 0;
	while(1)
	{
		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);

		//Detect Touch screen press
		boolean isTouch = TouchScreenHandler.isTouching();

		//Blink active text box cursor
		if(activeTextBox)
		TFT_TextBox.BlinkCursor();
		else
		ContactBox.BlinkCursor();

		//Debug Clear Screen
		if(clearScreenCounter >= 8)
		{
			ClearScreenMessageWrite();
			clearScreenCounter = 0;
		}


		if(isTouch && isReleased)
		{
			
			isReleased = false;
			
			//Touch debounce - ensure only the initial touch/single touch is recorded - touch screens are very bouncy
			if(TouchScreenHandler.pointDebounce())
			{

				//Get Touch location
				TSPoint p = TouchScreenHandler.getTouchPoint();

				//*****
				//Special touch location presses.
				//Quit text input
				if(CancelBox.isPressed(p.x, p.y))
				{
					boolean iscancel = queryCancel();
					
					if(iscancel)
					return;

					clearQueryBox();

					TouchKeyboard.DrawKeypad();

					SaveBox.ResetBox();
					SaveBox.PrintText("Save");
					
					CancelBox.ResetBox();
					CancelBox.PrintText("Cancel");

					SendBox.ResetBox();
					SendBox.PrintText("Send");

				}

				//Change active text box to Contact Box
				if(ContactBox.isPressed(p.x,p.y))
				activeTextBox = 0;
				//Change active text box to Main Text
				if(TFT_TextBox.isPressed(p.x,p.y))
				activeTextBox = 1;

				//save message on save box press
				if(SaveBox.isPressed(p.x,p.y))
				{
					char MessageTXT[TFT_TextBox.getTextLength()];
					char ContactTXT[ContactBox.getTextLength()];

					TFT_TextBox.getText(MessageTXT);
					ContactBox.getText(ContactTXT);

					String FileName = "";
					MessageHandler.SaveNewMessage(ContactTXT, MessageTXT, Message_Save, FileName);

					Serial.println("Message Saved");

					//Display Message on screen
					DisplayMessage("Message Saved");

					//Second Delay
					vTaskDelay((1000L * configTICK_RATE_HZ) / 1000L);

				}

				//Send Message Pressed
				if(SendBox.isPressed(p.x,p.y))
				{
					char MessageTXT[TFT_TextBox.getTextLength()];
					char ContactTXT[ContactBox.getTextLength()];



					//Get message contents and contact values
					TFT_TextBox.getText(MessageTXT);
					ContactBox.getText(ContactTXT);


					String FileName = "";
					//Save message to SD card with status for needing to be sent
					MessageHandler.SaveNewMessage(ContactTXT, MessageTXT, Message_Sending, FileName);

					//Check Message Que. If message que is full then save the message but do not add to que until que is emptied
					if(_GlobalMessageFileQueIndex < 10)
					_GlobalMessageFileQueIndex++;
					else
					{
						//Display Message on screen
						DisplayMessage("Error Sending Message. Message Que is full. Please wait until previous messages have been sent. Message Saved");
						//Second Delay
						vTaskDelay((1000L * configTICK_RATE_HZ) / 1000L);

						return;
					}
					
					_GlobalMessageFileQue[_GlobalMessageFileQueIndex] = FileName;


					//Flag global for sending new message
					_GlobalMyCommStatus = SendStatus_NewSend;

					//debug print
					Serial.println("Message Sending");

					//Display Message on screen
					DisplayMessage("Sending Message");

					//Second Delay
					vTaskDelay((1000L * configTICK_RATE_HZ) / 1000L);

					return;
				}
				//Query Key press if touch is inside keyboard area
				char KeyChar = TouchKeyboard.GetKeyPress(p);

				//Get key press and print to correct box
				if(KeyChar != NULL)
				{
					//Special key backspace to clear screen - test piece
					if(KeyChar == 0x08)
					{
						clearScreenCounter++;
					}else
					clearScreenCounter = 0;

					boolean textboxSpace = false;

					//Print to active text box
					if(activeTextBox)
					textboxSpace = TFT_TextBox.PrintText(KeyChar);
					else
					textboxSpace = ContactBox.PrintText(KeyChar);
					
					//Debug print to debug box
					String CharCount = "Char:" + String(TFT_TextBox._charCount);
					String LineCount = "\nLine:" + String(TFT_TextBox._lineCount);
					String ShiftMode = "\nShift:" + String(TouchKeyboard._keypadMode);

					boxLive = true;

					if(debugCharBoxEnable)
					{
						CharCountBox.ResetBox();
						
						//Serial.println("\nChar Count:" + String(TFT_TextBox._charCount));

						CharCountBox.PrintText(CharCount);
						CharCountBox.PrintText(LineCount);
						CharCountBox.PrintText(ShiftMode);
					}
					// 					Serial.println("\nChar Count:" + String(CharCountBox._charCount));
					// 					Serial.print("Cursor X:" + String(CharCountBox._cursorPosX));
					// 					Serial.println(", Cursor Y:" + String(CharCountBox._cursorPosY));
					//
				}

			}

		}else if(!isTouch)
		isReleased = true;

	}
}

void clearQueryBox()
{
	tft.fillRoundRect(0,180,240,320-180, 5, ILI9341_BLACK);
}

//Make sure of cancel request
boolean queryCancel()
{

	TFT_TextBoxClass QueryBox;
	TFT_TextBoxClass QueryYes;
	TFT_TextBoxClass QueryNo;
	//*************************************
	//Send Box for future sending gui element
	QueryBox.setup(&tft);
	QueryBox.setLocation(20,240-20,180,300);
	QueryBox.ResetBox();
	QueryBox.setFontSize(2);
	QueryBox.PrintText("Are you sure you want to cancel message?");
	
	//*************************************
	//Save Box for future saving of messages
	QueryYes.setup(&tft);
	QueryYes.setLocation(25,80,260,290);
	QueryYes.ResetBox();
	QueryYes.setFontSize(2);
	QueryYes.PrintText("Yes");
	
	//*************************************
	//Cancel Box - return from keypad program
	QueryNo.setup(&tft);
	QueryNo.setLocation(140,240-30,260,290);
	QueryNo.ResetBox();
	QueryNo.setFontSize(2);
	QueryNo.PrintText("No");

	while(1)
	{
		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);

		if(TouchScreenHandler.pointDebounce())
		{

			//Get Touch location
			TSPoint p = TouchScreenHandler.getTouchPoint();

			//*****
			//Special touch location presses.
			//Exit Message
			if(QueryNo.isPressed(p.x, p.y))
			return 0;

			if(QueryYes.isPressed(p.x, p.y))
			return 1;
		}
	}
}


//Clear screen and reset all text boxes to default state
void ClearScreenMessageWrite()
{
	ResetScreen();

	TouchKeyboard.DrawKeypad();
	TFT_TextBox.ResetBox();

	ContactBox.ResetBox();

	SendBox.ResetBox();
	SendBox.PrintText("Send");

	SaveBox.ResetBox();
	SaveBox.PrintText("Save");
	
	CancelBox.ResetBox();
	CancelBox.PrintText("Cancel");

	SendToBox.ResetBox();
	SendToBox.PrintText("Send To:");

}

void DisplayMessage(char *Message)
{
	TFT_TextBoxClass DebugBox;

	DebugBox.setup(&tft);
	DebugBox.setLocation(40,200,150,280);
	DebugBox.ResetBox();
	DebugBox.setFontSize(2);
	/*	DebugBox.PrintText("Sending Message To:");*/
	DebugBox.PrintText(Message);

}