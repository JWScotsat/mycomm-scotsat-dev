//*************************************************
//Setup boxes for contact editting/viewing
void GUIContactEditSetup(TFT_TextBoxClass *FirstNameHeader,TFT_TextBoxClass *LastNameHeader,TFT_TextBoxClass *EmailHeader,
TFT_TextBoxClass *TelHeader,TFT_TextBoxClass *CancelBox,TFT_TextBoxClass *SaveBox,TFT_TextBoxClass *FirstNameFill,TFT_TextBoxClass *LastNameFill,
TFT_TextBoxClass *EmailFill,TFT_TextBoxClass *TelFill)
{


	const int HeaderXSize = 60;
	const int FillStartX = HeaderXSize + 3;
	const int BoxYSize = 30;
	const int BoxYSpacer = 22;
	const int BoxYBuffer = 5;

	//Y Position tracking for drawing boxes
	int YPos = 0;
	int YPosEnd = 0;

	//*************************************
	//Save Box for future saving of messages
	SaveBox->setup(&tft);
	SaveBox->setLocation(65,125,164,190);
	SaveBox->ResetBox();
	SaveBox->setFontSize(2);
	SaveBox->PrintText("Save");

	//*************************************
	//Cancel Box - return from keypad program
	CancelBox->setup(&tft);
	CancelBox->setLocation(130,210,164,190);
	CancelBox->ResetBox();
	CancelBox->setFontSize(2);
	CancelBox->PrintText("Done");

	//Initial Y position
	YPos = BoxYSpacer;
	YPosEnd = BoxYSpacer + BoxYSize;

	//NameHeader - next to Name box
	LastNameHeader->setup(&tft);
	LastNameHeader->setLocation(0,HeaderXSize,YPos,YPosEnd);
	LastNameHeader->ResetBox();
	LastNameHeader->setFontSize(1);
	LastNameHeader->PrintText("Surname:");
	

	//Contact Name Box
	LastNameFill->setup(&tft);
	LastNameFill->setLocation(FillStartX,240,YPos,YPosEnd);
	LastNameFill->ResetBox();
	LastNameFill->setFontSize(2);
	LastNameFill->_cursorBlinkEnable = true;

	//Increameant Y Position to be "BoxYBuffer" pixels away from previous box
	YPos = YPosEnd +BoxYBuffer;
	YPosEnd = YPos + BoxYSize;

	//NameHeader - next to Name box
	FirstNameHeader->setup(&tft);
	FirstNameHeader->setLocation(0,HeaderXSize,YPos,YPosEnd);
	FirstNameHeader->ResetBox();
	FirstNameHeader->setFontSize(1);
	FirstNameHeader->PrintText("First   Name:");

	//Contact Name Box
	FirstNameFill->setup(&tft);
	FirstNameFill->setLocation(FillStartX,240,YPos,YPosEnd);
	FirstNameFill->ResetBox();
	FirstNameFill->setFontSize(2);
	FirstNameFill->_cursorBlinkEnable = true;

	//Increameant Y Position to be "BoxYBuffer" pixels away from previous box
	YPos = YPosEnd +BoxYBuffer;
	YPosEnd = YPos + BoxYSize;


	//Email header
	EmailHeader->setup(&tft);
	EmailHeader->setLocation(0,HeaderXSize,YPos,YPosEnd);
	EmailHeader->ResetBox();
	EmailHeader->setFontSize(1);
	EmailHeader->PrintText("Email:");

	//Contact Email Box
	EmailFill->setup(&tft);
	EmailFill->setLocation(FillStartX,240,YPos,YPosEnd);
	EmailFill->ResetBox();
	EmailFill->setFontSize(2);
	EmailFill->_cursorBlinkEnable = true;

	//Increameant Y Position to be "BoxYBuffer" pixels away from previous box
	YPos = YPosEnd +BoxYBuffer;
	YPosEnd = YPos + BoxYSize;

	//Phone number header
	TelHeader->setup(&tft);
	TelHeader->setLocation(0,HeaderXSize,YPos,YPosEnd);
	TelHeader->ResetBox();
	TelHeader->setFontSize(1);
	TelHeader->PrintText("Tel No.:");

	//Tel Box
	TelFill->setup(&tft);
	TelFill->setLocation(FillStartX,240,YPos,YPosEnd);
	TelFill->ResetBox();
	TelFill->setFontSize(2);
	TelFill->_cursorBlinkEnable = true;
}

//*************************************************
//Create new contact
void GUI_Contact_NewContact()
{
	ResetScreen();


	//*************************
	TFT_TextBoxClass FirstNameHeader;
	TFT_TextBoxClass LastNameHeader;
	TFT_TextBoxClass EmailHeader;
	TFT_TextBoxClass TelHeader;

	TFT_TextBoxClass CancelBox;
	TFT_TextBoxClass SaveBox;

	//*************************
	TFT_TextBoxClass FirstNameFill;
	TFT_TextBoxClass LastNameFill;
	TFT_TextBoxClass EmailFill;
	TFT_TextBoxClass TelFill;

	GUIContactEditSetup(&FirstNameHeader, &LastNameHeader,&EmailHeader,&TelHeader,&CancelBox, &SaveBox, &FirstNameFill, &LastNameFill, &EmailFill, &TelFill);



	//*************************
	TouchKeyboard.setup(&tft);

	TouchKeyboard.DrawKeypad();

	//Active text box control
	const int SurnameActive = 0;
	const int FirstNameActive = 1;
	const int EmailActive = 2;
	const int TelNoActive = 3;
	int activeTextBox = 0;

	int NewTextSelection = 0;

	boolean isReleased = true;

	while(1)
	{
		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);

		//Detect Touch screen press
		boolean isTouch = TouchScreenHandler.isTouching();

		//Active text box blinking cursor control
		switch(activeTextBox)
		{
			case SurnameActive: LastNameFill.BlinkCursor();break;
			case FirstNameActive: FirstNameFill.BlinkCursor();break;
			case EmailActive: EmailFill.BlinkCursor();break;
			case TelNoActive: TelFill.BlinkCursor();break;
		}


		if(isTouch && isReleased)
		{
			
			isReleased = false;
			
			//Touch debounce - ensure only the initial touch/single touch is recorded - touch screens are very bouncy
			if(TouchScreenHandler.pointDebounce())
			{

				//Get Touch location
				TSPoint p = TouchScreenHandler.getTouchPoint();

				//*****
				//Special touch location presses.
				//Quit text input
				if(CancelBox.isPressed(p.x, p.y))
				{
					boolean iscancel = queryCancel();
					
					if(iscancel)
					return;

					clearQueryBox();

				}

				//Touch detection of text input boxes
				if(LastNameFill.isPressed(p.x,p.y))
				activeTextBox = SurnameActive;

				if(FirstNameFill.isPressed(p.x,p.y))
				activeTextBox = FirstNameActive;

				if(EmailFill.isPressed(p.x,p.y))
				activeTextBox = EmailActive;
				if(TelFill.isPressed(p.x,p.y))
				activeTextBox = TelNoActive;


				//save contact on save box press
				if(SaveBox.isPressed(p.x,p.y))
				{
					char SurnameTXT[LastNameFill.getTextLength()];
					char FirstNameTXT[FirstNameFill.getTextLength()];
					char EmailTXT[EmailFill.getTextLength()];
					char TelTXT[TelFill.getTextLength()];

					LastNameFill.getText(SurnameTXT);
					FirstNameFill.getText(FirstNameTXT);
					EmailFill.getText(EmailTXT);
					TelFill.getText(TelTXT);

					String FileName = "";

					ContactHandler.SaveNewContact(FirstNameTXT,SurnameTXT,EmailTXT,TelTXT,0,FileName);

					Serial.println("Contact Saved");

					//Display Message on screen
					DisplayMessage("Contact Saved");

					//Second Delay
					vTaskDelay((1000L * configTICK_RATE_HZ) / 1000L);

				}

				//Query Key press if touch is inside keyboard area
				char KeyChar = TouchKeyboard.GetKeyPress(p);

				//Get key press and print to correct box
				if(KeyChar != NULL)
				{

					//Active text box fill keyboard press to
					switch(activeTextBox)
					{
						case SurnameActive: LastNameFill.PrintText(KeyChar);break;
						case FirstNameActive: FirstNameFill.PrintText(KeyChar);break;
						case EmailActive: EmailFill.PrintText(KeyChar);break;
						case TelNoActive: TelFill.PrintText(KeyChar);break;
					}

					


				}

			}

		}else if(!isTouch)
		isReleased = true;

	}
}
//*************************************************
//Contact Edit and View
void GUI_Contact_EditContact(int ContactSelected)
{
	ResetScreen();


	//*************************
	// Setup display text boxes
	TFT_TextBoxClass FirstNameHeader;
	TFT_TextBoxClass LastNameHeader;
	TFT_TextBoxClass EmailHeader;
	TFT_TextBoxClass TelHeader;

	TFT_TextBoxClass CancelBox;
	TFT_TextBoxClass SaveBox;

	//*************************
	TFT_TextBoxClass FirstNameFill;
	TFT_TextBoxClass LastNameFill;
	TFT_TextBoxClass EmailFill;
	TFT_TextBoxClass TelFill;

	GUIContactEditSetup(&FirstNameHeader, &LastNameHeader,&EmailHeader,&TelHeader,&CancelBox, &SaveBox, &FirstNameFill, &LastNameFill, &EmailFill, &TelFill);



	//*************************
	TouchKeyboard.setup(&tft);

	TouchKeyboard.DrawKeypad();

	//Active text box control
	const int SurnameActive = 0;
	const int FirstNameActive = 1;
	const int EmailActive = 2;
	const int TelNoActive = 3;
	int activeTextBox = 0;

	int NewTextSelection = 0;

	boolean isReleased = true;


	//Retrieve Contact File
	contactFile retrievedFile;
	ContactHandler.getContactFile(retrievedFile,ContactSelected);
	
	//Fill boxes with contact content
	FirstNameFill.PrintText(String(retrievedFile.Firstname),true);
	LastNameFill.PrintText(String(retrievedFile.Surname),true);
	EmailFill.PrintText(String(retrievedFile.Email),true);
	TelFill.PrintText(String(retrievedFile.TelPhone),true);

	while(1)
	{
		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);

		//Detect Touch screen press
		boolean isTouch = TouchScreenHandler.isTouching();

		//Active text box blinking cursor control
		switch(activeTextBox)
		{
			case SurnameActive: LastNameFill.BlinkCursor();break;
			case FirstNameActive: FirstNameFill.BlinkCursor();break;
			case EmailActive: EmailFill.BlinkCursor();break;
			case TelNoActive: TelFill.BlinkCursor();break;
		}


		if(isTouch && isReleased)
		{
			
			isReleased = false;
			
			//Touch debounce - ensure only the initial touch/single touch is recorded - touch screens are very bouncy
			if(TouchScreenHandler.pointDebounce())
			{

				//Get Touch location
				TSPoint p = TouchScreenHandler.getTouchPoint();

				//*****
				//Special touch location presses.
				//Quit text input
				if(CancelBox.isPressed(p.x, p.y))
				{
					boolean iscancel = queryCancel();
					
					if(iscancel)
					return;

					clearQueryBox();

				}

				//Touch detection of text input boxes
				if(LastNameFill.isPressed(p.x,p.y))
				activeTextBox = SurnameActive;

				if(FirstNameFill.isPressed(p.x,p.y))
				activeTextBox = FirstNameActive;

				if(EmailFill.isPressed(p.x,p.y))
				activeTextBox = EmailActive;
				if(TelFill.isPressed(p.x,p.y))
				activeTextBox = TelNoActive;


				//save contact on save box press
				if(SaveBox.isPressed(p.x,p.y))
				{
					//Last name is used for hashing file name. Must be filled in to a certain degree in order to fit has name
					if(LastNameFill.getTextLength() < 2)
					{
						//Display Message on screen
						DisplayMessage("Contact Surname Too Short. Can Not Save.");

						vTaskDelay((1000L * configTICK_RATE_HZ) / 1000L);

						//Redraw Keypad
						TouchKeyboard.DrawKeypad();

					}else
					{
						//Get text box contents
						//***
						char SurnameTXT[LastNameFill.getTextLength()];
						char FirstNameTXT[FirstNameFill.getTextLength()];
						char EmailTXT[EmailFill.getTextLength()];
						char TelTXT[TelFill.getTextLength()];

						SurnameTXT[0] = NULL;
						FirstNameTXT[0] = NULL;
						EmailTXT[0]= NULL;
						TelTXT[0] = NULL;
						

						LastNameFill.getText(SurnameTXT);
						FirstNameFill.getText(FirstNameTXT);
						EmailFill.getText(EmailTXT);
						TelFill.getText(TelTXT);

						//***

						String FileName = "";

						//Save file
						ContactHandler.SaveNewContact(FirstNameTXT,SurnameTXT,EmailTXT,TelTXT,0,FileName);

						Serial.println("Contact Saved:" + FileName);

						//Display Message on screen
						DisplayMessage("Contact Saved");

						//Second Delay
						vTaskDelay((1000L * configTICK_RATE_HZ) / 1000L);

						return;
					}
				}

				//Query Key press if touch is inside keyboard area
				char KeyChar = TouchKeyboard.GetKeyPress(p);

				//Get key press and print to correct box
				if(KeyChar != NULL)
				{

					//Active text box fill keyboard press to
					switch(activeTextBox)
					{
						case SurnameActive: LastNameFill.PrintText(KeyChar);break;
						case FirstNameActive: FirstNameFill.PrintText(KeyChar);break;
						case EmailActive: EmailFill.PrintText(KeyChar);break;
						case TelNoActive: TelFill.PrintText(KeyChar);break;
					}

					


				}

			}

		}else if(!isTouch)
		isReleased = true;

	}
}