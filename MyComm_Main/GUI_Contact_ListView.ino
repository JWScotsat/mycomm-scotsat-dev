//Test Code
//Loads contact list from SD card & allows creation of new contacts

#define SDError -1
#define SDEmpty 0
#define ContactListFound 1


//*************************************************
// Retreive contact list and print to screen
void ContactListTest()
{

	//Clear screen
	ResetScreen();

	//Array of text boxes to hold contact list
	TFT_TextBoxClass ContactList[numberofContactsperPage];

	//Character storage array for Contact Names
	char ContactListNames[300][MaxContactSize*2];
	
	//Retreive contact list from SDCard and fill ContactListNames
	int ListSize = BuildContactlist(ContactListNames);

	//Draw contact list on screen with individual text boxes
	displayContacts(ContactList,ContactListNames,ListSize);

	Serial.println("\nDone!");

	TFT_TextBoxClass CancelBox;

	//Cancel Box - return from keypad program
	CancelBox.setup(&tft);
	CancelBox.setLocation(130,210,300,320);
	CancelBox.ResetBox();
	CancelBox.setFontSize(1);
	CancelBox.PrintText("Cancel");


	TFT_TextBoxClass NewContactBox;

	//New Contact
	NewContactBox.setup(&tft);
	NewContactBox.setLocation(20,110,300,320);
	NewContactBox.ResetBox();
	NewContactBox.setFontSize(1);
	NewContactBox.PrintText("Add Contact");

	int ContactSelected = -1;

	while(1)
	{
		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);

		if(TouchScreenHandler.pointDebounce())
		{

			//Get Touch location
			TSPoint p = TouchScreenHandler.getTouchPoint();

			//*****
			//Special touch location presses.
			//Exit Contact List
			if(CancelBox.isPressed(p.x, p.y))
			return;

			//Run new contact screen for contact creation
			if(NewContactBox.isPressed(p.x, p.y))
			{
				GUI_Contact_NewContact();
				return;
			}
			

			for (int i = 0; i < ListSize; i++)
			{
				if(ContactList[i].isPressed(p.x,p.y))
				ContactSelected = i;
			}

			if(ContactSelected > -1)
			{
				GUI_Contact_EditContact(ContactSelected+1);

				//Clear screen
				ResetScreen();
				//Draw contact list on screen with individual text boxes
				displayContacts(ContactList,ContactListNames,ListSize);

				ContactSelected = -1;
				CancelBox.ResetBox();
				CancelBox.PrintText("Cancel");
			}
		}
	}

	return;
}



//*************************************************
//Retreive contact list from SDCard and fill ContactListNames
int BuildContactlist(char ContactListNames[300][MaxContactSize*2])
{
	//Debug box to print contact list contents
	TFT_TextBoxClass DebugBox;
	DebugBox.setup(&tft);
	DebugBox.setLocation(0,240,22,60);
	DebugBox.ResetBox();
	DebugBox.setFontSize(1);

	//Get number of contact files in SD card
	int numberofContacts = ContactHandler.FindNumberofFiles(".CON", true);

	//Empty Contact File
	if(numberofContacts == 0)
	DebugBox.PrintText("No Contacts Found.");
	else //Read contact list and save names to array
	{
		Serial.println("Number of contacts found:" + String(numberofContacts));

		for(int i = 0; i < numberofContacts; i++)
		{
			int textIndex = 0;

			contactFile TempFile;

			ContactHandler.getContactFile(TempFile,i+1);

			for(int j = 0; j < MaxContactSize; j++)
			{

				//Break at end of Name - all names are ended with a NULL
				if(TempFile.Firstname[j] == NULL || TempFile.Firstname[j]  == ';')
				break;

				ContactListNames[i][j] = TempFile.Firstname[j];
				
				DebugBox.PrintText(ContactListNames[i][j]);

				Serial.print(ContactListNames[i][j]);

				textIndex = j;
				
			}

			textIndex++;

			ContactListNames[i][textIndex] = 0x20;

			Serial.print(ContactListNames[i][textIndex]);
			DebugBox.PrintText(ContactListNames[i][textIndex]);

			textIndex++;

			for(int j =0 ; j < MaxContactSize; j++)
			{
				
				//Break at end of Name - all names are ended with a NULL
				if(TempFile.Surname[j] == NULL || TempFile.Surname[j]  == ';')
				{
					textIndex += j;
					break;
				}
				ContactListNames[i][j+textIndex] = TempFile.Surname[j];

				Serial.print(ContactListNames[i][j+textIndex]);
				
				DebugBox.PrintText(ContactListNames[i][j+textIndex]);

				
				
			}

			ContactListNames[i][textIndex] = NULL;


		}
		
	}


	return numberofContacts;

}

//*************************************************
// Print contact list to screen using individual Text Boxes
void displayContacts(TFT_TextBoxClass ContactList[numberofContactsperPage], char ContactListNames[300][MaxContactSize*2], int numberofContacts)
{
	Serial.println("Writing Contact Name to box:");

	//Print contact list to seperate contact boxes on screen
	for(int i = 0; i < numberofContacts && i < numberofContactsperPage ; i++) //
	{
		int minY = 30*(i+1)+40;
		ContactList[i].setup(&tft);
		ContactList[i].setLocation(10,230,minY+2,minY+30);
		ContactList[i].setFontSize(1);
		ContactList[i].ResetBox();

		for(int j = 0; j < MaxContactSize*2; j++)
		{
			
			if(ContactListNames[i][j] == NULL || ContactListNames[i][j] == ';')
			break;
			
			ContactList[i].PrintText(ContactListNames[i][j]);
			
			Serial.print(ContactListNames[i][j]);
		}
		Serial.println("");
	}

}
// *************************************************
// SD Debug, print directory to serial port
// String printDirectory(File dir, int numTabs)
// {
// 	
// 
// 	String Output = "File List: \n";
// 
// 	while(true)
// 	{
// 		
// 		File entry =  dir.openNextFile();
// 		if (! entry)
// 		{
// 			// no more files
// 			Serial.println("**nomorefiles**");
// 			break;
// 		}
// 		for (uint8_t i=0; i<numTabs; i++) {
// 			Output += '\t';
// 		}
// 
// 		Output += (entry.name());
// 		if (entry.isDirectory())
// 		{
// 			Output += ("/");
// 			printDirectory(entry, numTabs+1);
// 			} else {
// 			// files have sizes, directories do not
// 			Output += ("\t\t");
// 			Output += (entry.size(), DEC);
// 		}
// 		entry.close();
// 	}
// 
// 	Serial.print(Output);
// 	return Output;
// }

// 
// //*************************************************
// //Return contacts found inside contact file
// int GetContents(File *ContactFile, char ContactListNames[][30])
// {
// 
// 	size_t n;      // Length of returned field with delimiter.
// 	char str[MaxContactNameSize];  // Must hold longest field with delimiter and zero byte.
// 	// Read the file and print fields.
// 	
// 	int i = 0;
// 
// 	while(1)
// 	{
// 		n = readField(ContactFile, str, sizeof(str), ",\n");
// 
// 		// done if Error or at EOF.
// 		if (n == 0 || str[0] != '>') break;
// 
// 		// Print the type of delimiter.
// 		if (str[n-1] == ',' || str[n-1] == '\n')
// 		{
// 			Serial.print(str[n-1] == ',' ? F("comma: ") : F("endl:  "));
// 			
// 			// Remove the delimiter.
// 			str[n-1] = 0;
// 			str[n] = NULL;
// 
// 			//Serial.println("Saving String: n = " + String(n) + ", i =" + String(i));
// 
// 			for(int j = 0; j < n; j++)
// 			{
// 				ContactListNames[i][j] = str[j+1];
// 			}
// 
// 
// 
// 			i++;
// 			//return str;
// 		}
// 		else
// 		{
// 			// At eof, too long, or read error.  Too long is error.
// 			Serial.print(ContactFile->available() ? F("error: ") : F("eof:   "));
// 			str[n] = NULL;
// 			//Serial.println("Saving String: n = " + String(n) + ", i =" + String(i));
// 
// 			for(int j = 0; j < n; j++)
// 			{
// 				ContactListNames[i][j] = str[j+1];
// 			}
// 
// 			i++;
// 		}
// 
// 		// Print the field.
// 		Serial.println(str);
// 	}
// 	
// 	
// 	Serial.print("\n\n Contact List:");
// 
// 	for (int j = 0; j < i; j++)
// 	{
// 		for(int l = 0; l < MaxContactNameSize; l++)
// 		{
// 			if(ContactListNames[j][l] == NULL)
// 			break;
// 
// 			Serial.print(ContactListNames[j][l]);
// 		}
// 		Serial.println("");
// 	}
// 
// 	return i;
// }
// 
// //*************************************************
// 
// //Returns Character String from input file based on deliminator
// size_t readField(File* file, char* str, size_t size, char* delim)
// {
// 	char ch;
// 	size_t n = 0;
// 	while ((n + 1) < size && file->read(&ch, 1) == 1)
// 	{
// 		// Delete CR.
// 		if (ch == '\r') {
// 			continue;
// 		}
// 		str[n++] = ch;
// 		if (strchr(delim, ch)) {
// 			break;
// 		}
// 	}
// 	str[n] = '\0';
// 	return n;
// }
// 
// 
// //------------------------------------------------------------------------------
// #define errorHalt(msg) {Serial.println(F(msg)); while(1);}
// //------------------------------------------------------------------------------
