void GUI_Interface_Setup()
{
	TFT_TextBoxClass StartBox;

	tft.begin();
	tft.fillScreen(ILI9341_BLACK);
	tft.setRotation(0);

	TouchScreenHandler.setup(&tft);

	SDHandler.setupTFT(&tft);
	boolean SDAlive = SDHandler.setup(SD_CS);
	

	StartBox.setup(&tft);

	while (!SDAlive) {
		Serial.println(F("failed to access SD card!"));
		StartBox.setLocation(20,220,180,270);
		StartBox.ResetBox();
		StartBox.setFontSize(2);
		StartBox.PrintText(F("failed to access SD card!"));
		delay(3000);
	}

	Serial.println("OK!");



	
	GUILockScreen();
}


void GUI_Interface_Main()
{
	GUI_Interface_Setup();

	while(1)
	{

		switch(SplashButtonTest())
		{
			case 1: GUIMessages(); break;
			case 2: ContactListTest(); break;
			case 3: GUISettings(); break;
			case 4: GUILockScreen(); break;
			case 5: GUI_Sensors(); break;
			case 6: GUI_CheckMessages(); break;
		}

		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);
	}
}


//Clear screen
void ResetScreen()
{
	//tft.fillScreen(ILI9341_BLACK);
	tft.fillRect(0, 20, 240, 300, ILI9341_BLACK);
}

//Clear screen
void ResetScreenColour(uint16_t color)
{
	tft.fillScreen(color);
}

//Splash screen button test
int SplashButtonTest()
{
	int ImageSize = 70;
	int ImageSizeX = 70;
	int ImageSizeY = 82;
	int StartYOffset = 25;
	int StartXOffset = 240/4 - ImageSizeX/2;
	int ImageSpacingY = 10;
	int TextOffsetX = ImageSize + 12 + 15;
	int TextEndX = TextOffsetX + 140;
	int TextEndY = 40;

	TouchButtonClass ContactsBut;
	TouchButtonClass SettingsBut;
	TouchButtonClass MessagingBut;
	TouchButtonClass LockBut;
	TouchButtonClass SensorBut;
	TouchButtonClass CheckMessBut;

	//Clean screen from previous state
	ResetScreen();




	//New Message Keypad text and Image
	//tft.writeRect(StartXOffset,StartYOffset,ImageSize,ImageSizeY,);
	MessagingBut.setup(StartXOffset+(ImageSize/2), StartYOffset + (ImageSizeY/2), ImageSizeX, ImageSizeY, &tft, (uint16_t*)NewMail);
	MessagingBut.Draw();

	//Contact list Image and Touch Button
	//tft.writeRect(StartXOffset, StartYOffset + ImageSizeY + ImageSpacingY, ImageSize, ImageSizeY, (uint16_t*)NewContacts);
	ContactsBut.setup(StartXOffset+(ImageSizeX/2), StartYOffset + ImageSizeY + ImageSpacingY+(ImageSizeY/2), ImageSizeX, ImageSizeY, &tft, (uint16_t*)NewContacts);
	ContactsBut.Draw();

	//Received Messages Button Setup
	//tft.writeRect(StartXOffset,StartYOffset + ImageSizeY*2 + ImageSpacingY*2,ImageSizeX,ImageSizeY, (uint16_t*)SettingsIco);
	SettingsBut.setup(StartXOffset+(ImageSizeX/2), StartYOffset + ImageSizeY*2 + ImageSpacingY*2+(ImageSizeY/2), ImageSizeX, ImageSizeY, &tft, (uint16_t*)SettingsIco);
	SettingsBut.Draw();

	//Return to initial lock screen
	//tft.writeRect(240-50,320-50 ,40,50, (uint16_t*)LockIco);
	LockBut.setup(240-50 + (40/2), 320-50 + (50/2), 40, 50, &tft, (uint16_t*)LockIco);
	LockBut.Draw();

	//Run Sensor Reading
	//tft.writeRect(240-50,320-50 ,40,50, (uint16_t*)LockIco);
	SensorBut.setup(StartXOffset+(ImageSizeX/2) + 240/2, StartYOffset + (ImageSizeY/2), ImageSizeX, ImageSizeY, &tft, (uint16_t*)SensorIco);
	SensorBut.Draw();

	//Run Sensor Reading
	//tft.writeRect(240-50,320-50 ,40,50, (uint16_t*)LockIco);
	CheckMessBut.setup(StartXOffset+(ImageSizeX/2) + 240/2, StartYOffset + ImageSizeY + ImageSpacingY + (ImageSizeY/2), ImageSizeX, ImageSizeY, &tft, (uint16_t*)CheckMessagesIco);
	CheckMessBut.Draw();

	while(1)
	{
		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);

		if(TouchScreenHandler.pointDebounce())
		{
			TSPoint p = TouchScreenHandler.getTouchPoint();

			if(MessagingBut.isPressed(p))
			return 1;

			if(ContactsBut.isPressed(p))
			return 2;

			if(SettingsBut.isPressed(p))
			return 3;
			
			if(LockBut.isPressed(p))
			return 4;

			if(SensorBut.isPressed(p))
			return 5;

			if(CheckMessBut.isPressed(p))
			return 6;
		}
	}

}

//*****************************************************************

//Lock Screen - initial boot up screen for unlocking device
void GUILockScreen()
{
	debugPrintln("Drawing Lock Screen");

	ResetScreen();
	

	//SDHandler.bmpDraw("logo.bmp", 0, 20);
	tft.writeRect(0, 22, 240, 104, (uint16_t*)logo);

	TouchButtonClass UnlockButton;
	//Draw unlock button on screen
	//tft.writeRect((240-96)/2, 160, 96, 120, (uint16_t*)UnlockBut);
	UnlockButton.setup(240/4 + (96/2), 160+(120/2), 96, 120, &tft, (uint16_t*)UnlockBut);
	UnlockButton.Draw();

	
	TFT_TextBoxClass MessageBox;

	//Message Box - print message content
	MessageBox.setup(&tft);
	MessageBox.setLocation(20,210,80,290);
	//MessageBox.ResetBox();
	MessageBox.setFontSize(2);
	boolean MessagePrinted = false;




	// Wait for screen touch to continue
	while(1)
	{

		//debugPrintln("Lock Screen -Inside while touch loop");

		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);

		//Detect Touch
		if(TouchScreenHandler.pointDebounce())
		{
			TSPoint p = TouchScreenHandler.getTouchPoint();
			//Query touch inside box
			if(UnlockButton.isPressed(p))
			{
				Serial.println("Start Box Is Touched!");
				break;
			}
		}

		//Handle Emergency Mode Display
		if(_GlobalMyCommStatus == Status_EmergencyMode)
		{
			if(!MessagePrinted)
			{
				MessageBox.ResetBox();
				MessageBox.PrintText("MyComm In Emergency Mode! Attempting to send for help");
				MessagePrinted = true;
			}
		}


	}

}

//*****************************************************************

//Check message button
// Sets Global status to send status message and retreieve waiting messages
// Prints to screen help message that MyComm is checking messages
// Does not conflict with other iridium sends
void GUI_CheckMessages()
{

	TFT_TextBoxClass MessageBox;

	//Message Box - print message content
	MessageBox.setup(&tft);
	MessageBox.setLocation(20,210,80,290);
	MessageBox.ResetBox();
	MessageBox.setFontSize(2);

	if(_GlobalIrid_IridBusy == false || _GlobalMyCommStatus != ReceiveStatus_Checking)
	{
		MessageBox.PrintText("Checking with Satellite for New Messages. This may take some time and will run in the background. New messages will appear when ready");

		_GlobalMyCommStatus = ReceiveStatus_Checking;
	}else
	{
		MessageBox.PrintText("Modem Currently Busy. Please try again later");
	}

	vTaskDelay((3000 * configTICK_RATE_HZ) / 1000L);
}