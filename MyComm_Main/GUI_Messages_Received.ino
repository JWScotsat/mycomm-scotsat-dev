//Find number of files on sd card root with given fileextension
int FindNumberofFiles(String FileExtension, boolean DebugPrint)
{
	File SDRoot = SD.open("/", FILE_READ);

	if(DebugPrint)
	Serial.print("File List:");
	
	int NoMessages = 0;

	while(true)
	{
		
		File entry =  SDRoot.openNextFile();
		if (! entry) {
			// no more files
			//Serial.println("**nomorefiles**");
			entry.close();
			break;
		}

		if(DebugPrint)
		Serial.println(entry.name());

		String fileName(entry.name());

		if(fileName.endsWith(FileExtension))
		{
			NoMessages++;
		}

		entry.close();
	}

	SDRoot.close();

	return NoMessages;
}


//TODO: code should open a list of saved messages, display most recent and allow for message reading
void GUIMessages()
{
	int numberofMessagesperpage = 7;

	//Clean screen from previous state
	ResetScreen();

	if(_GlobalMyCommStatus == ReceiveStatus_NewMessage)
	_GlobalMyCommStatus = IridStatus_Clear;


	if(FindNumberofFiles(".RMG", 0) < 1)
	{
		//Create a list of mock messages
		for(int i = 0; i < 10; i++)
		{
			char Contact[50];
			char TestMessage[300];
			sprintf(Contact, "Contact%02d@mail.com", i);
			sprintf(TestMessage, "This is a new test message. Number in list should be: %02d", i);

			String FileName;

			MessageHandler.SaveNewMessage(Contact, TestMessage, 2, FileName);
		}
	}

	Serial.print("No. of Message File Found:" + String(FindNumberofFiles(".RMG", 0)));


	messageFile readBackMessage;

	int numberofMessagesFound = FindNumberofFiles(".RMG", 0);

	String newFileName = String(numberofMessagesFound-1) + ".RMG";

	//Array of text boxes to hold contact list
	TFT_TextBoxClass MessageList[numberofMessagesperpage];
	
	printMessageList(MessageList, numberofMessagesperpage, numberofMessagesFound);




	//Cancel Box - return from keypad program
	TouchButtonClass CancelBox;
	//Draw unlock button on screen
	CancelBox.setup(15+(50/2), 320-62+(50/2), 50, 62, &tft, (uint16_t*)ReturnIco);
	CancelBox.Draw();

	TouchButtonClass ComposeBut;
	//Compose Button - Launches Keypad
	ComposeBut.setup(240-65 + (61/2), 320-75+(70/2), 61, 70, &tft, (uint16_t*)ComposeIco);
	ComposeBut.Draw();


	int MessageSelected = -1;

	while(1)
	{
		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);

		if(TouchScreenHandler.pointDebounce())
		{

			//Get Touch location
			TSPoint p = TouchScreenHandler.getTouchPoint();

			//*****
			//Special touch location presses.
			//Exit Contact List
			if(CancelBox.isPressed(p))
			return;

			//Compose new message. Display keypad loop
			if(ComposeBut.isPressed(p))
			{
				KeypadTest_loop();

				// Rebuild display of message list
				printMessageList(MessageList, numberofMessagesperpage, numberofMessagesFound);
				MessageSelected = -1;

				CancelBox.Draw();
				ComposeBut.Draw();
			}

			for (int i = 0; i < numberofMessagesperpage; i++)
			{
				if(MessageList[i].isPressed(p.x,p.y))
				MessageSelected = i;
			}

			if(MessageSelected > -1)
			{

				//Display selected message and contents on screen
				displayMessage(MessageSelected, numberofMessagesFound);

				// Rebuild display of message list
				printMessageList(MessageList, numberofMessagesperpage, numberofMessagesFound);
				MessageSelected = -1;
				CancelBox.Draw();
				ComposeBut.Draw();
			}
		}
	}



	return;
}

void printMessageList(TFT_TextBoxClass *MessageList, int numberofMessagesperpage, int numberofMessagesFound)
{
	//Clean screen from previous state
	ResetScreen();


	for (int i = 0; i < numberofMessagesperpage; i++)
	{
		if(i > numberofMessagesFound)
		break;

		messageFile readBackMessage;


		String newFileName = String(numberofMessagesFound-1-i) + ".RMG";

		MessageHandler.ReadBackMessage(readBackMessage,  newFileName.c_str());

		String TextPrint =String(numberofMessagesFound-1-i)+ ":"+ String(readBackMessage.Contact) + "  " + String(readBackMessage.Date);

		int minY = 30*(i+1);



		MessageList[i].setup(&tft);
		MessageList[i].setLocation(10,230,minY+2,minY+30);
		MessageList[i].setFontSize(1);


		//Message display box colour depending on message status
		switch(readBackMessage.status)
		{
			case Message_Sent: MessageList[i].setBackGroundColour(ILI9341_GREEN); break;
			case Message_Save: MessageList[i].setBackGroundColour(ILI9341_YELLOW); break;
			case Message_Sending: MessageList[i].setBackGroundColour(ILI9341_ORANGE); break;
			case Message_Received: MessageList[i].setBackGroundColour(ILI9341_CYAN); break;
		}


		MessageList[i].ResetBox();
		MessageList[i].PrintText(TextPrint);

	}
}


//**********************************************************************
//Displays message contents from SD Card
// allows for resending or deleting of message
void displayMessage(int MessageSelected, int numberofMessages)
{
	int Selectedmessage = numberofMessages - MessageSelected;

	messageFile readBackMessage;

	ResetScreen();


	String newFileName = String(Selectedmessage - 1) + ".RMG";

	MessageHandler.ReadBackMessage(readBackMessage,  newFileName.c_str());

	TFT_TextBoxClass ContactBox;
	TFT_TextBoxClass MessageBox;



	TFT_TextBoxClass CancelBox;
	
	//Touch button for ability to resend the message
	TFT_TextBoxClass ResendBox;
	boolean ResendActive = false;

	//Cancel Box - return from program
	CancelBox.setup(&tft);
	CancelBox.setLocation(130,195,295,320);
	CancelBox.ResetBox();
	CancelBox.setFontSize(2);
	CancelBox.PrintText("Done");

	//Resend Box - Button only appears when message is marked as fail to send
	if(readBackMessage.status == Message_Sending && _GlobalMessageFileQueIndex == -1)
	{
		ResendActive = true;
		ResendBox.setup(&tft);
		ResendBox.setLocation(60,125,295,320);
		ResendBox.ResetBox();
		ResendBox.setFontSize(2);
		ResendBox.PrintText("Retry");
	}


	//Message Box - print message content
	MessageBox.setup(&tft);
	MessageBox.setLocation(5,210,80,290);
	MessageBox.ResetBox();
	MessageBox.setFontSize(2);
	MessageBox.PrintText("Message:" + String(readBackMessage.Message));

	//ContactBox - print contact content
	ContactBox.setup(&tft);
	ContactBox.setLocation(5,210,25,75);
	ContactBox.ResetBox();
	ContactBox.setFontSize(1);
	ContactBox.PrintText("Contact:");
	ContactBox.setFontSize(1);
	ContactBox.PrintText(String(readBackMessage.Contact) + " -- " + String(readBackMessage.Date));

	while(1)
	{
		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);
		
		if(TouchScreenHandler.pointDebounce())
		{

			//Get Touch location
			TSPoint p = TouchScreenHandler.getTouchPoint();

			//*****
			//Special touch location presses.
			//Exit Contact List
			if(CancelBox.isPressed(p.x, p.y))
			return;

			if(ResendBox.isPressed(p.x, p.y))
			{
				//Check Message Que. If message que is full then save the message but do not add to que until que is emptied
				if(_GlobalMessageFileQueIndex < 10)
				_GlobalMessageFileQueIndex++;
				else
				{
					//Display Message on screen
					DisplayMessage("Error Sending Message. Message Que is full. Please wait until previous messages have been sent. Message Saved");
					//Second Delay
					vTaskDelay((1000L * configTICK_RATE_HZ) / 1000L);

					return;
				}
				
				_GlobalMessageFileQue[_GlobalMessageFileQueIndex] = newFileName;


				//Flag global for sending new message
				_GlobalMyCommStatus = SendStatus_NewSend;
			}

		}
	}
	
}

