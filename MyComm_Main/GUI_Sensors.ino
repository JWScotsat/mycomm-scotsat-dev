void GUI_Sensors()
{
	TFT_GraphsClass BarGraphTest;
	TFT_TextBoxClass PhotoResisBox;
	TFT_TextBoxClass PhotoResisValBox;


	TFT_GraphsClass BarGraphTest2;
	TFT_TextBoxClass RandomBox;
	TFT_TextBoxClass RandomValBox;

	TFT_TextBoxClass SendDataBut;
	TFT_TextBoxClass RecordSDBut;
	TFT_TextBoxClass ReturnBut;
	
	ResetScreen();

	PhotoResisBox.setup(&tft);
	PhotoResisBox.setLocation(0,50,40,70);
	PhotoResisBox.setFontSize(1);
	PhotoResisBox.setBackGroundColour(ILI9341_BLACK);
	PhotoResisBox.setTextColour(ILI9341_WHITE);
	PhotoResisBox.ResetBox();
	PhotoResisBox.PrintText("Light Level:");

	PhotoResisValBox.setup(&tft);
	PhotoResisValBox.setLocation(210,240,40,70);
	PhotoResisValBox.setFontSize(1);
	PhotoResisValBox.setBackGroundColour(ILI9341_BLACK);
	PhotoResisValBox.setTextColour(ILI9341_WHITE);
	PhotoResisValBox.ResetBox();
	PhotoResisValBox.PrintText(":");

	BarGraphTest.setup(&tft, 55, 40, 200, 70, 0, 10);
	BarGraphTest.drawBorder();



	//******************************
		RandomBox.setup(&tft);
		RandomBox.setLocation(0,50,90,120);
		RandomBox.setFontSize(1);
		RandomBox.setBackGroundColour(ILI9341_BLACK);
		RandomBox.setTextColour(ILI9341_WHITE);
		RandomBox.ResetBox();
		RandomBox.PrintText("Random Val:");

		RandomValBox.setup(&tft);
		RandomValBox.setLocation(210,240,90,120);
		RandomValBox.setFontSize(1);
		RandomValBox.setBackGroundColour(ILI9341_BLACK);
		RandomValBox.setTextColour(ILI9341_WHITE);
		RandomValBox.ResetBox();
		RandomValBox.PrintText(":");

		BarGraphTest2.setup(&tft, 55, 90, 200, 120, 0, 500);
		BarGraphTest2.drawBorder();
	//******************************
	unsigned long startimer = millis();



		//******************************
		SendDataBut.setup(&tft);
		SendDataBut.setLocation(10,80,290,320);
		SendDataBut.setFontSize(1);
		//SendDataBut.setBackGroundColour(ILI9341_BLACK);
		//SendDataBut.setTextColour(ILI9341_WHITE);
		SendDataBut.ResetBox();
		SendDataBut.PrintText("Send Snapshot");

			//******************************
		RecordSDBut.setup(&tft);
		RecordSDBut.setLocation(90,160,290,320);
		RecordSDBut.setFontSize(1);
		//RecordSDBut.setBackGroundColour(ILI9341_BLACK);
		//RecordSDBut.setTextColour(ILI9341_WHITE);
		RecordSDBut.ResetBox();
		RecordSDBut.PrintText("Record To SD");

		//******************************
		ReturnBut.setup(&tft);
		ReturnBut.setLocation(170,240,290,320);
		ReturnBut.setFontSize(1);
		//ReturnBut.setBackGroundColour(ILI9341_BLACK);
		//ReturnBut.setTextColour(ILI9341_WHITE);
		ReturnBut.ResetBox();
		ReturnBut.PrintText("Return");

	

	while(1)
	{
		//Main rest
		if(vRestMainTask)vTaskDelay((vMainTaskDelay * configTICK_RATE_HZ) / 1000L);


		if(TouchScreenHandler.pointDebounce())
		{
			TSPoint p = TouchScreenHandler.getTouchPoint();

			if(ReturnBut.isPressed(p.x, p.y))
			return;
		}


		if(millis() - startimer > 500)
		{
			//Serial.println("Photo Read:" + String(PhotoResistorRead()));
			startimer = millis();

			int newReadingPhoto = PhotoResistorRead();
			int randomVal = random(500);
			
			BarGraphTest.update(newReadingPhoto);
			BarGraphTest2.update(randomVal);

			PhotoResisValBox.PrintText(":" + String(newReadingPhoto), 1);
			RandomValBox.PrintText(":" + String(randomVal), 1);

			if(newReadingPhoto < 3)
			digitalWrite(debugLEDPin, HIGH);
			else
			digitalWrite(debugLEDPin, LOW);

		}
	}
}
