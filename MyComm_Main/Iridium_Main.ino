//Main Iridium Task
// Uses Iridium Handler class to enable Send/Receive of messages and other modem functions
// Updates global variables for Iridium status and saves/retrieves files using Message Handler class



Iridium_HandlerClass IridHandler; // Iridium hanlder to simplfy uses of iridum modem

//*****************************************************************************************
//Main FreeRTOS task
// Continually updates signal and message waiting status
// Handles global flags for send/receive iridium messages
// Rests 500ms per loop

static void vIridium_Main_Task(void *pvParameters)
{
	IridHandler._IridiumSerialDebug = IridiumSerialDebug;

	IridHandler.Setup(); // Setup handler class with IirdiumSBD class instance
	



	while(1)
	{
		//Serial.println("Running Iridium Task");

		Iridium_UpdateStatus();

		if(_GlobalMyCommStatus == ReceiveStatus_Failed)
		_GlobalMyCommStatus = ReceiveStatus_Checking;

		//New attempt to send message
		if(_GlobalMyCommStatus == SendStatus_NewSend)
		_GlobalMyCommStatus = SendStatus_Sending;
		

		if(_GlobalMyCommStatus == SendStatus_Sending)
		{
			_GlobalIrid_IridBusy = true;
			if(_GlobalSignalQuality >= 2)
			{
				Iridium_SendMessageQue();
				_GlobalIrid_IridBusy = false;
			}
		}
		
		if(_GlobalMyCommStatus == ReceiveStatus_Checking)
		{
			_GlobalIrid_IridBusy = true;
			if(_GlobalSignalQuality >= 2)
			{
				Iridium_CheckMessages();
				_GlobalIrid_IridBusy = false;
			}
		}

		//Handle Emergency Status Mode
		if(_GlobalMyCommStatus == Status_EmergencyMode)
		{
			_GlobalIrid_IridBusy = true;
			Iridium_EmergencySend();
			_GlobalIrid_IridBusy = false;
			
		}

		//Sleep task 300ms
		vTaskDelay((501 * configTICK_RATE_HZ) / 1000L);

	}
}

//Unused ISDBCallback - can be used for contuing tasks when iridium is transmitting
// bool ISBDCallback()
// {
// 	vTaskDelay((500L * configTICK_RATE_HZ) / 1000L);
// 	return true;
// }
//*************************************************************************************

//Update signal strength and message waiting globals depending on Iridium status
void Iridium_CheckMessages()
{
	//Send New Message
	_GlobalIrid_IridBusy = true;

	do
	{
		//Create status message to send in order to receive messages
		String StatusMessage = "+MyComm Status Check. Battery;USB, Temp;" + _GlobalInternalTemp + ", TimeUp;" + String(millis()/1000) + ", Signal;" + String(_GlobalSignalQuality) + ", Version;" + CodeVersion +",";
		StatusMessage += NULL;

		Serial.println("Sending Status Update to Server:" + StatusMessage);

		Iridium_DelayMultiSend();//Delay/Wait until conditions are met for sending message

		int SendResult = IridHandler.SendMessageReceive(StatusMessage); //Send/Receive messages

		if(SendResult == SentNoReceive)
		_GlobalMyCommStatus = ReceiveStatus_NoWaiting; //Flag No messages
		else if(SendResult == SentAndReceived)
		{
			Iridium_SaveReceivedMessage(); //save new message file
			_GlobalMyCommStatus = ReceiveStatus_NewMessage; //Flag new message
		}else
		{
			Serial.println("Irid Task - Failed check for messages. Retrying");
			_GlobalMyCommStatus = ReceiveStatus_Failed;
			break;
		}
		
	} while (IridHandler._MessagesWaiting > 0);

	_GlobalIrid_IridBusy = false;
}

//*************************************************************************************

//Update signal strength and message waiting globals depending on Iridium status
void Iridium_UpdateStatus()
{


	IridHandler.UpdateStatus(); //get latest values from handler

	int signalQuality = IridHandler._SignalStrength;
	int messageWaiting = IridHandler._MessagesWaiting;

	//Update Global Irdium Signal Strength
	if(_GlobalSignalQuality != signalQuality)
	{
		noInterrupts();
		_GlobalSignalQuality = signalQuality;
		interrupts();
		
	}
	
	//Update Global Messages Waiting
	if(_GlobalMessageWaiting != messageWaiting)
	{
		noInterrupts();
		_GlobalMessageWaiting = messageWaiting;
		interrupts();
		
	}
}

//*************************************************************************************

//Send Messages in message que and save any new messages
int Iridium_SendMessageQue()
{


	if(_GlobalQuedMessage > _GlobalMessageFileQueIndex)
	_GlobalQuedMessage = 0;


	_GlobalIrid_IridBusy = true;
	Serial.println("Attempting to send new message");

	//Send all messages in message que
	for(int i = _GlobalQuedMessage; i <= _GlobalMessageFileQueIndex; i++)
	{
		
		String FileName = _GlobalMessageFileQue[i];


		//Grab File Contents for sending message
		noInterrupts();
		String NewMessage = MessageHandler.MessageToIridString(FileName.c_str()); //Get message file converted to string for iridium
		interrupts();

		Iridium_DelayMultiSend(); //Delay/Wait until conditions are met for sending message

		int SendResult = IridHandler.SendMessageReceive(NewMessage); //Send message over iridium

		
		if(SendResult == SendFail)
		{
			Serial.println("+Irid Task - message failed to send. Auto-retrying. Que:" + String(_GlobalQuedMessage) + "/" + String(_GlobalMessageFileQueIndex));

			_GlobalQuedMessage = i;

			return SendFail;
		}
		else if(SendResult == SentNoReceive)
		{
			noInterrupts();
			MessageHandler.UpdateMessageStatus(Message_Sent, FileName.c_str());//Update message qued to show it was sent
			interrupts();

			_GlobalMyCommStatus = SendStatus_Sent;
		}
		else if(SendResult == SentAndReceived)
		{
			noInterrupts();
			MessageHandler.UpdateMessageStatus(Message_Sent, FileName.c_str()); //Update message qued to show it was sent
			interrupts();

			Iridium_SaveReceivedMessage(); //Save received message to file
			

			_GlobalMyCommStatus = SendStaus_SentAndReceived;
		}
		


	}

	_GlobalMessageFileQueIndex = -1;

	_GlobalQuedMessage = 0;

	//_GlobalMessageFileQueIndex = -1;

	//Handle waiting messages by sending status messages
	if(IridHandler._MessagesWaiting > 0)
	Iridium_CheckMessages();

	return SendStatus_Sent;
}

//*************************************************************************************


//Save Received message to file from RXBuffer
// Return action complete
boolean Iridium_SaveReceivedMessage()
{
	messageFile NewIncomming;

	NewIncomming.Contact[0] = NULL;
	
	boolean MessageReturn = IridHandler.ParseReceiveToChar(NewIncomming.Message);
	
	String FileName;

	//Save new message
	noInterrupts();
	MessageHandler.SaveNewMessage(NewIncomming.Contact,NewIncomming.Message, Message_Received, FileName);
	interrupts();

	debugPrintln("New Iridium Message Saved as:" + FileName);

	return true;
}

//*************************************************************************************
//Delay send attempts in order to give iridium modem time to re-charge capacitor.
// Delay may cause signal strength to drop below 2. Wait until signal strength returns
void Iridium_DelayMultiSend()
{
	Serial.println("+Irid Task - Delay multi send checking!");

	IridHandler.UpdateStatus();

	while(!IridHandler.isReadyToTransmit())
	{
		IridHandler.UpdateStatus();
		vTaskDelay((501 * configTICK_RATE_HZ) / 1000L);
	}

	while(IridHandler._SignalStrength < 2)
	{
		Iridium_UpdateStatus();
		vTaskDelay((501 * configTICK_RATE_HZ) / 1000L);

		Serial.println("+Irid Task - Waiting for signal strength before sending");
	}

	Serial.println("+Irid Task - Time and signal conditions met. Attempting send!");
	return;
}

//*************************************************************************************

//Send Emergency Message
// Handle any waiting messages
// Continually attempt to send
int Iridium_EmergencySend()
{

	_GlobalIrid_IridBusy = true;
	Serial.println("Attempting to send Emergency message");

	int SendResult = SendFail;

	while(SendResult == SendFail)
	{
		//Grab File Contents for sending message
		String NewMessage = "!MyComm Emergency Message - Flag Help!"; //Get message file converted to string for iridium
		NewMessage += NULL;

		Iridium_DelayMultiSend(); //Delay/Wait until conditions are met for sending message

		SendResult = IridHandler.SendMessageReceive(NewMessage); //Send message over iridium

		
		if(SendResult == SendFail)
		{
			Serial.println("+Irid Task - message failed to send. Auto-retrying");
		}
		else if(SendResult == SentNoReceive)
		{
			_GlobalMyCommStatus = Emergency_Sent;
		}
		else if(SendResult == SentAndReceived)
		{
			Iridium_SaveReceivedMessage(); //Save received message to file

			_GlobalMyCommStatus = Emergency_SentAndReceived;
		}
		

	}
	//Handle waiting messages by sending status messages
	if(IridHandler._MessagesWaiting > 0)
	Iridium_CheckMessages();

	return SendStatus_Sent;
}

//*************************************************************************************