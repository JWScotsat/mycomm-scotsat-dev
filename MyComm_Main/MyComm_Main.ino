
#include "FreeRTOS/FreeRTOS_ARM.h"
#include <SPI.h>
#include <ILI9341_t3.h>
#include "MyComm_Status_Header.h"
#include "TouchScreen.h"
#include "Supporting Classes/TFT Libraries/TouchScreenHandler.h"
#include "Supporting Classes/TFT Libraries/TFT_TextBox.h"
#include "Supporting Classes/TFT Libraries/TouchKeyboard.h"
#include "Supporting Classes/TFT Libraries/TouchButton.h"
#include "Supporting Classes/TFT Libraries/TFT_Graphs.h"
#include "Supporting Classes/Iridium Classes/Iridium_Handler.h"
#include "ProgramImageHeader.h"
#include "Supporting Classes/GUI_TopInfoBar.h"
#include "Storage Functions/MessageHandler.h"
#include "Storage Functions/ContactHandler.h"


#include <SD.h>
#include "Storage Functions/SDHandler.h"
#define SD_CS 14

#define MaxContactNameSize 30
#define numberofContactsperPage 10





// The display uses hardware SPI, plus #9 & #10
#define TFT_CS 10
#define TFT_DC  9
ILI9341_t3 tft = ILI9341_t3(TFT_CS, TFT_DC);


const int MaxScreenX = 240;
const int MaxScreenY = 320;

//Global Iridium Status Values
volatile int _GlobalSignalQuality = -1;
volatile int _GlobalMessageWaiting = -1;
boolean _GlobalIrid_IridBusy = false;

//Global Sensor Data
String _GlobalInternalTemp = "-1";

//Global Message Que - Allows for sending of multiple messages in a que
String _GlobalMessageFileQue[10];
volatile int _GlobalMessageFileQueIndex = -1;

//Store the current message in que to send
int	_GlobalQuedMessage = 0;

#define debugPrintln(x) Serial.println(x)
#define IridiumSerialDebug true //Rockblock Serial data is output to serial port

#define debugShowPointEnable true
#define debugPrintCoords false
const String CodeVersion = "V0.301-B";

volatile unsigned long count = 0;

// handle for tasks
TaskHandle_t gui;
TaskHandle_t topInfoHandle;
TaskHandle_t iridiumHandle;
TaskHandle_t debugHandle;
TaskHandle_t EmergencyHandle;

//Global variables for handling new message sending states
volatile int _GlobalMyCommStatus = 0;


#define vMainTaskDelay 5L
#define vRestMainTask true

#define debugLEDPin 21

void setup() {
	

	Serial.begin(9600);
	delay(5000);
	Serial.print(F("Scotsat MyComm - Free RTOS Version: ")); Serial.println(CodeVersion);

	//dbTest();

	pinMode(debugLEDPin, OUTPUT);

	//***************

	// create Top Info bar task
	xTaskCreate(vTopInfoGUITask,
	"TopGUI",
	configMINIMAL_STACK_SIZE + 500,
	NULL,
	tskIDLE_PRIORITY + 2,
	&topInfoHandle);
	
	Serial.print(F("Top Info GUI Task Created: "));  Serial.println((pcTaskGetTaskName(topInfoHandle)));
	//***************

	delay(500);

	//***************
	// create main gui task
	xTaskCreate(vMainGUITask,
	"MainGUI",
	configMINIMAL_STACK_SIZE + 10500,
	NULL,
	tskIDLE_PRIORITY + 3,
	&gui);

	Serial.print(F("Main GUI Task Created: ")); Serial.println(pcTaskGetTaskName(gui));
	//***************

	delay(200);

	// create main Iridium task
	xTaskCreate(vIridium_Main_Task,
	"IridT",
	configMINIMAL_STACK_SIZE + 500,
	NULL,
	tskIDLE_PRIORITY + 1,
	&iridiumHandle);

	Serial.print(F("Main Irid Task Created: ")); Serial.println(pcTaskGetTaskName(iridiumHandle));

	//***************
	// create print task
	delay(200);

	xTaskCreate(vPrintTask,
	"DebugT",
	configMINIMAL_STACK_SIZE + 120,
	NULL,
	tskIDLE_PRIORITY + 0,
	&debugHandle);

	Serial.print(F("Debug Task Created: ")); Serial.println(pcTaskGetTaskName(debugHandle));

	delay(500);
	//***************

	// create Top Info bar task
	xTaskCreate(vTaskEmergencyMode,
	"T_Emerg",
	configMINIMAL_STACK_SIZE + 120,
	NULL,
	tskIDLE_PRIORITY + 1,
	&EmergencyHandle);
	
	Serial.print(F("Emergency Task Created: "));  Serial.println((pcTaskGetTaskName(EmergencyHandle)));
	//***************



	//***************
	// start FreeRTOS
	vTaskStartScheduler();

	// should never return
	Serial.println(F("Die"));
	while(1);
}



void loop()
{

	// 		while(1) {
	// 			// must insure increment is atomic
	// 			// in case of context switch for print
	noInterrupts();
	count++;
	interrupts();
	// 		}

}






//------------------------------------------------------------------------------
// high priority main GUI interface
static void vMainGUITask(void *pvParameters) {
	
	GUI_Interface_Main();
	
	// Sleep for 50 milliseconds.
	vTaskDelay((50L * configTICK_RATE_HZ) / 1000L);

	
}

//------------------------------------------------------------------------------
static void vPrintTask(void *pvParameters) {
	while (1) {
		// Sleep for one second.
		vTaskDelay(configTICK_RATE_HZ);
		
		// Print count for previous second.
		Serial.print(F("Low Priority Count: "));
		Serial.print(count);
		
		// Print unused stack for threads.
		Serial.print(F(", Unused Stack: "));
		Serial.print(uxTaskGetStackHighWaterMark(gui));
		Serial.print(' ');
		Serial.print(uxTaskGetStackHighWaterMark(topInfoHandle));
		Serial.print(' ');
		Serial.print(uxTaskGetStackHighWaterMark(iridiumHandle));
		Serial.print(' ');
		Serial.print(uxTaskGetStackHighWaterMark(debugHandle));
		Serial.print(' ');
		Serial.println(uxTaskGetStackHighWaterMark(xTaskGetIdleTaskHandle()));
		
		// Zero count.
		count = 0;
	}
}
//------------------------------------------------------------------------------
