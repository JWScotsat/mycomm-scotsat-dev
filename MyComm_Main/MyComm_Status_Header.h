#ifndef _MyComm_Status_h
#define _MyComm_Status_h

//Status states for iridium
#define SendStaus_SentAndReceived 7
#define SendStatus_NewSend 1
#define SendStatus_Sending 2
#define SendStatus_Sent 3
#define IridStatus_Clear 0
#define SendStatus_Error -1
#define ReceiveStatus_NewMessage 4
#define ReceiveStatus_Checking 5
#define ReceiveStatus_NoWaiting 6
#define ReceiveStatus_Failed -2

#define Status_EmergencyMode 8
#define Emergency_Sent 81
#define Emergency_SentAndReceived 82



#endif