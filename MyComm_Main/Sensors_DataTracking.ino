int PhotoResistorRead()
{
	const int pinNumb = A8;

	static boolean FirstRead = true;

	if(FirstRead)
	{
		pinMode(pinNumb, INPUT);
		FirstRead = false;
	}

	int result = analogRead(pinNumb);

	result = map(result, 0, 4096, 0, 10);

	return result;
}

//**************************************************************************
//Internal temp read
String InternalTempRead()
{
	unsigned long sensorValue = analogRead(38);  // variable to store the value coming from the sensor
	float average = 0.0;

	for (byte counter =0;counter<255;counter++){
		sensorValue += analogRead(38);
	}

	average= (float)sensorValue/255.0;

	float C = (25.0 + 0.17083 * (2454.19 - average*4))/8.3336;
	
	String OutString = String(C);


	//Serial.println("+Temp:" + OutString);
	//Serial.println(OutString);

	return OutString;
}