//
//
//

#include "ContactHandler.h"

//*****************************
//Save Contact to new file
boolean ContactHandlerClass::SaveNewContact(char *FirstName,char *Surname,char *Email,char *TelPhone, int status, String &FileName)
{
	if(_DebugPrint)
	Serial.println("Saving Contact as SD File");

	//Create local copy of contact
	contactFile newContact;

	//Load first name into newContact
	CopyArray(newContact.Firstname, FirstName, MaxContactSize);

	//Load Surname into newContact
	CopyArray(newContact.Surname, Surname, MaxContactSize);

	//Load Email into newContact
	CopyArray(newContact.Email, Email, MaxContactSize);

	//Load Telephone number into newContact
	CopyArray(newContact.TelPhone, TelPhone, MaxContactSize);


	newContact.status = status;


	//Create new file name
	String newFileName = CreateFileName(Surname);

	if(_DebugPrint)
	Serial.println("New message name:" + newFileName);

	//Create file and copy struct to file

	File newFile = SD.open(newFileName.c_str(), FILE_WRITE);
	newFile.seek(0);

	byte  *buff = (byte *) &newContact;

	//Write file to sd card
	newFile.write(buff, sizeof(contactFile));

	newFile.close();


	if(_DebugPrint)
	Serial.println("Closing File.");

	//Return Fail name the message was saved as
	FileName = newFileName;
	
	return SD.exists(newFileName.c_str());
}

//*****************************
//Readback contact file
boolean ContactHandlerClass::ReadBackContact(struct contactFile &ReturnContact,const char *FileName)
{
	if(_DebugPrint)
	Serial.println("Reading Back SD File:" + String(FileName));

	if(SD.exists(FileName))
	{
		File newFile = SD.open(FileName, FILE_READ);

		if ( !newFile ) // if test.txt not found, notify and change state
		{
			Serial.print( "Unable to open for read: " );
			Serial.println( FileName );
			return false;
		}

		newFile.seek(0);

		byte  *Readbuff = (byte *) &ReturnContact;

		newFile.read(Readbuff, sizeof(contactFile));

		newFile.close();

		return true;

	}else if(_DebugPrint)
	Serial.println("File not found!");

	return false;
}

//************************************************************************************************

//Copy contents of one array to another
void ContactHandlerClass::CopyArray(char *ArrayOut,char *ArrayIn, int MaxSize)
{
	for(int i = 0; i < MaxSize; i++)
	{
		if(ArrayIn[i] == NULL || i == MaxSize -1) //Finish copy conditions
		break;

		ArrayOut[i] = ArrayIn[i];
		ArrayOut[i+1] = NULL;
	}
}

//************************************************************************************************

//Create file name from intial letters or surname + hash key
String ContactHandlerClass::CreateFileName(char *Surname)
{
	String FileName = "";

	for (int i = 0; i<2; i++)
	{
		if(Surname[i] == NULL)
		break;
		FileName += Surname[i];
	}

	FileName += "-HASH.con";

	return FileName;
}

//*****************************
//SD card search. Gives number of files with FileExtension ending
int ContactHandlerClass::FindNumberofFiles(String FileExtension, boolean DebugPrint)
{
	File SDRoot = SD.open("/", FILE_READ);

	if(DebugPrint)
	Serial.print("File List:");
	
	int NoMessages = 0;

	while(true)
	{
		
		File entry =  SDRoot.openNextFile();
		if (! entry) {
			// no more files
			//Serial.println("**nomorefiles**");
			entry.close();
			break;
		}

		if(DebugPrint)
		Serial.println(entry.name());

		String fileName(entry.name());

		if(fileName.endsWith(FileExtension))
		{
			NoMessages++;
		}

		entry.close();
	}

	SDRoot.close();

	return NoMessages;
}

//*****************************

//Get Contact based on index number for total contact files stored on sd card
boolean ContactHandlerClass::getContactFile(struct contactFile &ReturnContact, int ContactNumber)
{
	File SDRoot = SD.open("/", FILE_READ);

	if(_DebugPrint)
	{
		Serial.println("Looking for file number:" + String(ContactNumber));
		//Serial.print("File List:");
	}
	int NoMessages = 0;

	while(true)
	{
		
		File entry =  SDRoot.openNextFile();
		if (! entry) {
			// no more files
			//Serial.println("**nomorefiles**");
			entry.close();
			break;
		}

		// 		if(_DebugPrint)
		// 		Serial.println(entry.name());

		String fileName(entry.name());

		entry.close();

		if(fileName.endsWith(".CON"))
		{
			NoMessages++;

			if(NoMessages == ContactNumber)
			{
				contactFile TempFile;

				Serial.println("Selected Message file found. File Name:" + fileName);
				ReadBackContact(TempFile,fileName.c_str());
				SDRoot.close();
				
				ReturnContact = TempFile;

				return true;
			}

			
		}

		
	}

	SDRoot.close();

	Serial.println("File number not found:" + String(ContactNumber));

	return false;
}
ContactHandlerClass ContactHandler;

