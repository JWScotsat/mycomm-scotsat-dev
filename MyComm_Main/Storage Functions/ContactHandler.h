// ContactHandler.h

#ifndef _CONTACTHANDLER_h
#define _CONTACTHANDLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#ifndef SDClass
#include <SD.h>
#endif

#define MaxContactSize 50
#define MaxMessageSize 301

#define Message_Save 3
#define Message_Sending 4
#define Message_Sent 2
#define Message_Received 1
#define Message_FailedToSend -1


struct contactFile{
	char Firstname[MaxContactSize];
	char Surname[MaxMessageSize];
	char Email[MaxContactSize];
	char TelPhone[MaxContactSize];
	int status = 0;
};

class ContactHandlerClass
{
	protected:
	contactFile _CurrentReadFile;
	
	boolean _DebugPrint = true;

	void CopyArray(char *ArrayOut,char *ArrayIn, int MaxSize); //Copy array contents from one array to another
	String CreateFileName(char *Surname); //Create hashed file name for new contact

	public:
	boolean SaveNewContact(char *FirstName,char *Surname,char *Email,char *TelPhone, int status, String &FileName);
	boolean ReadBackContact(struct contactFile &ReturnContact,const char *FileName);
	int FindNumberofFiles(String FileExtension, boolean DebugPrint);

	boolean getContactFile(struct contactFile &ReturnContact, int ContactNumber);//Get contact based on number order

};

extern ContactHandlerClass ContactHandler;

#endif

