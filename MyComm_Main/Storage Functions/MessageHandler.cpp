//
//
//

#include "MessageHandler.h"

//*****************************
//Setup to plass SD class as it should already be configured
void MessageHandlerClass::setup(SDClass *SD)
{
	//_SD = SD;
}

//*****************************
//Save message to new file
boolean MessageHandlerClass::SaveNewMessage(char *Contact, char *Message, int status, String &FileName)
{
	if(_DebugPrint)
	Serial.println("Saving Message as SD File:");

	//Get number of Receive Message files - basic implementation does not take into account deleted files for naming
	int nextMessageNumber = FindNumberofFiles(".RMG", 0);

	if(_DebugPrint)
	Serial.println("New message number:" + String(nextMessageNumber));

	//Create local copy of message
	messageFile newMessage;

	for(int i = 0; i < MaxContactSize; i++)
	newMessage.Contact[i] = Contact[i];
	
	for(int i = 0; i < MaxMessageSize; i++)
	newMessage.Message[i] = Message[i];

	newMessage.status = status;

	//Create random date for now
	int m = random(1, 12);
	int d = random(1, 31);
	sprintf(newMessage.Date, "2016-%02d-%02d", m, d);

	//Create new file name
	String newFileName = String(nextMessageNumber) + ".RMG";

	if(_DebugPrint)
	Serial.println("New message name:" + newFileName);

	//Create file and copy struct to file
	File newFile = SD.open(newFileName.c_str(), FILE_WRITE);
	newFile.seek(0);

	byte  *buff = (byte *) &newMessage;

	newFile.write(buff, sizeof(messageFile));

	newFile.close();


	if(_DebugPrint)
	Serial.println("Closing File.");
	//Return Fail name the message was saved as
	FileName = newFileName;
	
	return _SD.exists(newFileName.c_str());
}

//*****************************

boolean MessageHandlerClass::ReadBackMessage(struct messageFile &ReturnMessage,const char *FileName)
{
	if(_DebugPrint)
	Serial.println("Reading Back SD File:" + String(FileName));

	if(SD.exists(FileName))
	{
		File newFile = SD.open(FileName, FILE_READ);

		if ( !newFile ) // if test.txt not found, notify and change state
		{
			Serial.print( "Unable to open for read: " );
			Serial.println( FileName );
			return false;
		}

		newFile.seek(0);

		byte  *Readbuff = (byte *) &ReturnMessage;

		newFile.read(Readbuff, sizeof(messageFile));

		newFile.close();

		return true;

	}else if(_DebugPrint)
	Serial.println("File not found!");

	return false;
}

//*****************************

//Convert message file to iridium string
String  MessageHandlerClass::MessageToIridString(const char *FileName)
{
	messageFile FileHolder;

	ReadBackMessage(FileHolder, FileName);

	String NewMessage = "";

	for(int i = 0; i < MaxContactSize; i++)
	{
		if(FileHolder.Contact[i] != NULL)
		NewMessage += FileHolder.Contact[i];
		else
		break;
	}

	NewMessage += "\t>";

	for(int i = 0; i < MaxMessageSize; i++)
	{
		if(FileHolder.Message[i] != NULL)
		NewMessage += FileHolder.Message[i];
		else
		break;
	}
	NewMessage += NULL;

	return NewMessage;
}

//*****************************
//Update Message with new status
boolean MessageHandlerClass::UpdateMessageStatus(int NewStatus,const char *FileName)
{
	messageFile FileHolder;

	ReadBackMessage(FileHolder, FileName);

	FileHolder.status = NewStatus;

	SaveMessage(FileHolder.Contact,FileHolder.Message, FileHolder.status, FileName);

	return true;
}

//*****************************

int MessageHandlerClass::FindNumberofFiles(String FileExtension, boolean DebugPrint)
{
	File SDRoot = SD.open("/", FILE_READ);

	if(DebugPrint)
	Serial.print("File List:");
	
	int NoMessages = 0;

	while(true)
	{
		
		File entry =  SDRoot.openNextFile();
		if (! entry) {
			// no more files
			//Serial.println("**nomorefiles**");
			entry.close();
			break;
		}

		if(DebugPrint)
		Serial.println(entry.name());

		String fileName(entry.name());

		if(fileName.endsWith(FileExtension))
		{
			NoMessages++;
		}

		entry.close();
	}

	SDRoot.close();

	return NoMessages;
}

//*****************************

//*****************************
//Save message to existing file
boolean MessageHandlerClass::SaveMessage(char *Contact, char *Message, int status, String FileName)
{
	if(_DebugPrint)
	Serial.println("Saving Message as SD File:");

	//Create local copy of message
	messageFile newMessage;

	for(int i = 0; i < MaxContactSize; i++)
	newMessage.Contact[i] = Contact[i];
	
	for(int i = 0; i < MaxMessageSize; i++)
	newMessage.Message[i] = Message[i];

	newMessage.status = status;

	//Create random date for now
	int m = random(1, 12);
	int d = random(1, 31);
	sprintf(newMessage.Date, "2016-%02d-%02d", m, d);

	//Create new file name
	String newFileName = FileName;

	if(_DebugPrint)
	Serial.println("New message name:" + newFileName);

	//Create file and copy struct to file
	File newFile = SD.open(newFileName.c_str(), FILE_WRITE);
	newFile.seek(0);

	byte  *buff = (byte *) &newMessage;

	newFile.write(buff, sizeof(messageFile));

	if(_DebugPrint)
	Serial.println("Closing File.");

	newFile.close();

	//Return File name the message was saved as
	FileName = newFileName;
	
	return _SD.exists(newFileName.c_str());
}

//*****************************


MessageHandlerClass MessageHandler;