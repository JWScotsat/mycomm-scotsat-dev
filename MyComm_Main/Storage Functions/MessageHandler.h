// MessageHandler.h

#ifndef _MESSAGEHANDLER_h
#define _MESSAGEHANDLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#ifndef SDClass
#include <SD.h>
#endif

#define MaxContactSize 50
#define MaxMessageSize 301

#define Message_Save 3
#define Message_Sending 4
#define Message_Sent 2
#define Message_Received 1
#define Message_FailedToSend -1


struct messageFile{
	char Contact[MaxContactSize];
	char Message[MaxMessageSize];
	int status = 0;
	char Date[10];
};


class MessageHandlerClass
{
	protected:
	messageFile _CurrentReadMessage;
	
	boolean _DebugPrint = true;

	SDClass _SD;

	public:
	void setup(SDClass *SD);
	boolean SaveNewMessage(char *Contact, char *Message, int status, String &FileName);
	boolean SaveMessage(char *Contact, char *Message, int status, String FileName);
	boolean ReadBackMessage(struct messageFile &ReturnMessage,const char *FileName);
	String MessageToIridString(const char *FileName); //Convert message file to iridium string
	boolean UpdateMessageStatus(int NewStatus,const char *FileName); //Update Message with new status
	int FindNumberofFiles(String FileExtension, boolean DebugPrint);
};

extern MessageHandlerClass MessageHandler;
#endif

