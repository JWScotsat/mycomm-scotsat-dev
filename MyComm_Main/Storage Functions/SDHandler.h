// SDHandler.h

#ifndef _SDHANDLER_h
#define _SDHANDLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#ifndef ILI9341_BLACK
#include <ILI9341_t3.h>
#endif

#if defined USE_TEENSY3_OPTIMIZED_CODE
	#include <SDOptimised/SDOptimised.h>
#else
	#include <SD.h>
#endif



class SDHandlerClass
{
 protected:
	ILI9341_t3 *_tft;
	int _SD_CS;
	unsigned long _SDTimeout = 2000;
	uint32_t read32(File &f);
	uint16_t read16(File &f);
	int _BUFFPIXEL = 80;

 public:
	void setupTFT(ILI9341_t3 *tft);
	boolean setup(int SD_CS);
	void bmpDraw(const char *filename, uint8_t x, uint16_t y);
};

extern SDHandlerClass SDHandler;

#endif

