// 
// File dbFile;
// 
// #define TABLE_SIZE 8192
// 
// // The number of demo records that should be created.  This should be less
// // than (TABLE_SIZE - sizeof(EDB_Header)) / sizeof(Contacts).  If it is higher,
// // operations will return EDB_OUT_OF_RANGE for all records outside the usable range.
// #define RECORDS_TO_CREATE 10
// char* db_name = "/db/edb_test.db";
// 
// //
// // struct Contacts {
// // 	int id;
// // 	int temperature;
// // }
// // Contacts;
// 
// struct Contact {
// 	int Name;
// 	int Email;
// 	int TelNo;
// } Contacts;
// 
// int recno;
// 
// void writer(unsigned long address, byte data)
// {
// 	dbFile.seek(address);
// 	dbFile.write(data);
// 	dbFile.flush();
// }
// 
// byte reader(unsigned long address)
// {
// 	dbFile.seek(address);
// 	byte b = dbFile.read();
// 	return b;
// }
// 
// 
// // Create an EDB object with the appropriate write and read handlers
// EDB db(&writer, &reader);
// 
// void dbTest()
// {
// 
// 	Serial.println(" Extended Database Library + External SD CARD storage demo");
// 	Serial.println();
// 
// 	delay(100);
// 
// 	randomSeed(analogRead(0));
// 
// 	if (!SD.begin(SD_CS)) {
// 	
// 		Serial.println("No SD-card.");
// 		delay(100);
// 		return;
// 	}
// 
// 	// Check dir for db files
// 	if (!SD.exists("/db")) {
// 		Serial.println("Dir for Db files does not exist, creating...");
// 		delay(100);
// 		SD.mkdir("/db");
// 		delay(100);
// 	}
// 
// 	if (SD.exists(db_name)) {
// 	delay(100);
// 		dbFile = SD.open(db_name, FILE_WRITE);
// 		delay(100);
// 		// Sometimes it wont open at first attempt, especially after cold start
// 		// Let's try one more time
// 		if (!dbFile) {
// 			dbFile = SD.open(db_name, FILE_WRITE);
// 		}
// 
// 		if (dbFile) {
// 			Serial.print("opening current table... ");
// 			EDB_Status result = db.open(0);
// 			if (result == EDB_OK) {
// 				Serial.println("DONE");
// 				} else {
// 				Serial.println("ERROR");
// 				Serial.println("Did not find database in the file " + String(db_name));
// 				Serial.print("Creating new table... ");
// 				db.create(0, TABLE_SIZE, (unsigned int)sizeof(Contacts));
// 				Serial.println("DONE");
// 				return;
// 			}
// 			} else {
// 			Serial.println("Could not open file " + String(db_name));
// 			return;
// 		}
// 		} else {
// 		Serial.print("Creating table... ");
// 		// create table at with starting address 0
// 		dbFile = SD.open(db_name, FILE_WRITE);
// 		db.create(0, TABLE_SIZE, (unsigned int)sizeof(Contacts));
// 		Serial.println("DONE");
// 	}
// 
// 	recordLimit();
// 	countRecords();
// 	createRecords(RECORDS_TO_CREATE);
// 	countRecords();
// 	selectAll();
// 	deleteOneRecord(RECORDS_TO_CREATE / 2);
// 	countRecords();
// 	selectAll();
// 	appendOneRecord(RECORDS_TO_CREATE + 1);
// 	countRecords();
// 	selectAll();
// 	insertOneRecord(RECORDS_TO_CREATE / 2);
// 	countRecords();
// 	selectAll();
// 	updateOneRecord(RECORDS_TO_CREATE);
// 	selectAll();
// 	countRecords();
// 	deleteAll();
// 	Serial.println("Use insertRec() and deleteRec() carefully, they can be slow");
// 	countRecords();
// 	for (int i = 1; i <= 20; i++) insertOneRecord(1);  // inserting from the beginning gets slower and slower
// 	countRecords();
// 	for (int i = 1; i <= 20; i++) deleteOneRecord(1);  // deleting records from the beginning is slower than from the end
// 	countRecords();
// 
// 	dbFile.close();
// }
// 
// 
// 
// 
// // utility functions
// 
// void recordLimit()
// {
// 	Serial.print("Record Limit: ");
// 	Serial.println(db.limit());
// }
// 
// void deleteOneRecord(int recno)
// {
// 	Serial.print("Deleting recno: ");
// 	Serial.println(recno);
// 	db.deleteRec(recno);
// }
// 
// void deleteAll()
// {
// 	Serial.print("Truncating table... ");
// 	db.clear();
// 	Serial.println("DONE");
// }
// 
// void countRecords()
// {
// 	Serial.print("Record Count: ");
// 	Serial.println(db.count());
// }
// 
// void createRecords(int num_recs)
// {
// 	Serial.print("Creating Records... ");
// 	for (int recno = 1; recno <= num_recs; recno++)
// 	{
// 		Contacts.Name =	 recno;
// 		Contacts.TelNo = random(1, 125);
// 		EDB_Status result = db.appendRec(EDB_REC Contacts);
// 		if (result != EDB_OK) printError(result);
// 	}
// 	Serial.println("DONE");
// }
// 
// void selectAll()
// {
// 	for (int recno = 1; recno <= db.count(); recno++)
// 	{
// 		EDB_Status result = db.readRec(recno, EDB_REC Contacts);
// 		if (result == EDB_OK)
// 		{
// 			Serial.print("Recno: ");
// 			Serial.print(recno);
// 			Serial.print(" ID: ");
// 			Serial.print(Contacts.Name);
// 			Serial.print(" Temp: ");
// 			Serial.println(Contacts.TelNo);
// 		}
// 		else printError(result);
// 	}
// }
// 
// void updateOneRecord(int recno)
// {
// 	Serial.print("Updating record at recno: ");
// 	Serial.print(recno);
// 	Serial.print("... ");
// 	Contacts.Name = 1234;
// 	Contacts.TelNo = 4321;
// 	EDB_Status result = db.updateRec(recno, EDB_REC Contacts);
// 	if (result != EDB_OK) printError(result);
// 	Serial.println("DONE");
// }
// 
// void insertOneRecord(int recno)
// {
// 	Serial.print("Inserting record at recno: ");
// 	Serial.print(recno);
// 	Serial.print("... ");
// 	Contacts.Name = recno;
// 	Contacts.TelNo = random(1, 125);
// 	EDB_Status result = db.insertRec(recno, EDB_REC Contacts);
// 	if (result != EDB_OK) printError(result);
// 	Serial.println("DONE");
// }
// 
// void appendOneRecord(int id)
// {
// 	Serial.print("Appending record... ");
// 	Contacts.Name = id;
// 	Contacts.TelNo = random(1, 125);
// 	EDB_Status result = db.appendRec(EDB_REC Contacts);
// 	if (result != EDB_OK) printError(result);
// 	Serial.println("DONE");
// }
// 
// void printError(EDB_Status err)
// {
// 	Serial.print("ERROR: ");
// 	switch (err)
// 	{
// 		case EDB_OUT_OF_RANGE:
// 		Serial.println("Recno out of range");
// 		break;
// 		case EDB_TABLE_FULL:
// 		Serial.println("Table full");
// 		break;
// 		case EDB_OK:
// 		default:
// 		Serial.println("OK");
// 		break;
// 	}
// }