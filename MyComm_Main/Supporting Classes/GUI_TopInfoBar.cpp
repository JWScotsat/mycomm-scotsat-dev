// 
// 
// 

#include "GUI_TopInfoBar.h"

//**************************************************************************
//Top info setup
// Passes tft element to top info class
void GUI_TopInfoBarClass::setup(ILI9341_t3 tft)
{
	_tft = &tft;

	_TopInfoBar.setup(&tft);
	_TopInfoBar.setLocation(0,240,0,20);
	_TopInfoBar.setFontSize(1);
	_TopInfoBar.setBackGroundColour(ILI9341_YELLOW);
	_TopInfoBar.setTextColour(ILI9341_BLACK);
}


//**************************************************************************
//Bar colour is based on signal status 
void GUI_TopInfoBarClass::UpdateStatusColour(int Status)
{
	//Colour defined for different signal levels
	switch (Status)
	{
		case 0: _BoxColour = ILI9341_YELLOW;break;
		case 1: _BoxColour = ILI9341_GREENYELLOW;break;
		case 2: _BoxColour = ILI9341_GREEN;break;
		case 3:	_BoxColour = ILI9341_GREEN;break;
		case 4:	_BoxColour = ILI9341_GREEN;break;
		case 5: _BoxColour = ILI9341_GREEN;break;
		default: _BoxColour = ILI9341_RED;break;
	}
}


//**************************************************************************
//Bar text is based on MyComm Status
void GUI_TopInfoBarClass::UpdateText(int signalQuality, int MessagesWaiting, int IridiumStatus, String CodeVersion,int MessageQueLength, int MessageQuePosition )
{
	//Message string
	_TopMessage = "";

	//Center Top Message Changes based on Message State
	switch(IridiumStatus)
	{
		case IridStatus_Clear: _TopMessage = ("Signal: " + String(signalQuality) + "(" + String(MessagesWaiting) + ")" + " MyComm:" + CodeVersion +"    :" +String(millis()/1000)); break;
		case SendStatus_NewSend: _TopMessage = ("Signal: " + String(signalQuality) + "(" + String(MessagesWaiting) + ")" + " -New Message Send-  :" +String(millis()/1000)); break;
		case SendStatus_Sending: _TopMessage = ("Signal: " + String(signalQuality)+ "(" + String(MessagesWaiting) + ")" + " -Sending" + String(MessageQuePosition+1) + "/" + String(MessageQueLength+1) +"- :" +String(millis()/1000)); break;
		case SendStatus_Sent: _TopMessage = ("Signal:" + String(signalQuality)+ "(" + String(MessagesWaiting) + ")" + " -Message Sent-   :" +String(millis()/1000)); break;
		case SendStatus_Error: _TopMessage = ("Signal:" + String(signalQuality)+ "(" + String(MessagesWaiting) + ")" + " -Message Error-   :" +String(millis()/1000)); break;
		case ReceiveStatus_Checking: _TopMessage = ("Signal:" + String(signalQuality)+ "(" + String(MessagesWaiting) + ")" + " -Checking Messages-: " +String(millis()/1000)); break;
		case ReceiveStatus_NoWaiting:_TopMessage = ("Signal:" + String(signalQuality)+ "(" + String(MessagesWaiting) + ")" + " -No New Messages-:" +String(millis()/1000)); break;
		case ReceiveStatus_NewMessage:_TopMessage = ("Signal:" + String(signalQuality)+ "(" + String(MessagesWaiting) + ")" + " -New Message!- :" +String(millis()/1000)); break;
		case ReceiveStatus_Failed:_TopMessage = ("Signal:" + String(signalQuality)+ "(" + String(MessagesWaiting) + ")" + " -Check Failed- :" +String(millis()/1000)); break;
		case Status_EmergencyMode: _TopMessage = ("Signal:" + String(signalQuality)+ "(" + String(MessagesWaiting) + ")" + " -Emergency Mode!- :" +String(millis()/1000)); break;
		default: _TopMessage = ("Signal: " + String(signalQuality) + "(" + String(MessagesWaiting) + ")" + " MyComm:" + (CodeVersion) +"    :" +String(millis()/1000)); break;
	}
}


//**************************************************************************
//Write latest bar to screen
void GUI_TopInfoBarClass::UpdateScreen()
{
	
	//Update colour of background to screen
	_TopInfoBar.setBackGroundColour(_BoxColour);

	//Clear box to be written to
	_TopInfoBar.ResetBox();

	//Top info fill text
	_TopInfoBar.PrintText(_TopMessage);

}



GUI_TopInfoBarClass GUI_TopInfoBar;

