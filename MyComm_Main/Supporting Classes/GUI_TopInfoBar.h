// GUI_TopInfoBar.h

#ifndef _GUI_TOPINFOBAR_h
#define _GUI_TOPINFOBAR_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#ifndef ILI9341_BLACK
#include <ILI9341_t3.h>
#endif

#ifndef _TFT_TEXTBOX_h
#include "TFT Libraries/TFT_TextBox.h"
#endif

#ifndef _IRIDIUM_HANDLER_h
#include "Iridium Classes/Iridium_Handler.h"
#endif

class GUI_TopInfoBarClass
{
 protected:
 ILI9341_t3 *_tft;
 TFT_TextBoxClass _TopInfoBar;
 uint16_t _BoxColour = 0;
 String _TopMessage = "";
 public:
	void init();

	void setup(ILI9341_t3 tft);//Main setup of class. Passes ILI9341 element

	void UpdateStatusColour(int Status); //Set colour of box based on signal status

	void UpdateText(int signalQuality, int MessagesWaiting, int IridiumStatus, String CodeVersion,int MessageQueLength, int MessageQuePosition ); //Update Text based on status info

	void UpdateScreen();//Write latest contents to screen

	String getContents() {return _TopMessage;};//Return String contents


};

extern GUI_TopInfoBarClass GUI_TopInfoBar;

#endif

