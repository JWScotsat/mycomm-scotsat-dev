//Iridium Handler Class
// Handles interactions with IridiumSBD library. Holds state values and general communications
//
#include "Iridium_Handler.h"

IridiumSBD isbd(IridiumSerialPort, IridSleepPin); //Iridium Rockblock library from Arduiana.org
//***********************************************************************

//Main setup. Initialise values and serial port to connect with Iridium Modem
boolean Iridium_HandlerClass::Setup()
{
	IridiumSerialPort.begin(IridiumSerialBaud); //Initialise serial port
	//isbd.attachConsole(Serial);
	isbd.setPowerProfile(1); //Set power Mode
	isbd.begin(); //Startup Iridium module

		//Debug infor printed to serial port from Rockblock module
		if(_IridiumSerialDebug)
		{
			isbd.attachConsole(Serial);
			isbd.attachDiags(Serial);
		}

	return true; //Setup complete
}

//***********************************************************************

//Update Status Values
// Returns pass/fail result
boolean Iridium_HandlerClass::UpdateStatus()
{
	//Prevent querying status when iridium transmission is being attempted
	if(!_IsTransmitting)
	{
		int signalQuality = -1;

		//Get signal quality
		int err = isbd.getSignalQuality(signalQuality);

		//Get number of messages waiting on satellite
		int messageWaiting = isbd.getWaitingMessageCount();

		//Handle error condition
		//TODO: better handling or error
		if (err != 0)
		{
			Serial.print("+Irid Handler -SignalQuality failed: error ");
			Serial.println(err);
			return false;
		}

		//Update class stores
		_SignalStrength = signalQuality;
		_MessagesWaiting = messageWaiting;
	}

	return true;
}

//***********************************************************************

//Send message and report back if message has been received
int Iridium_HandlerClass::SendMessageReceive(String MessageString)
{

	//Delay requirement before attempting multiple sends - too many sends causes the iridium to lose power due to capacitor discharge
	if(!isReadyToTransmit())
	{
	Serial.println("+Irid Handler - Send delay. Last send was:" + String(millis() - _LastSendStamp));

	while(!isReadyToTransmit())delay(1000);
	}


	if(_debugPrint)
		Serial.println(("+Irid Handler -Send Message Contents:" + MessageString));

	_rxBuffer[0] = NULL;

	size_t bufferSize = sizeof(_rxBuffer);

	_IsTransmitting = true; //Class flag to prevent other calls from communicating with the modem

	ClearBuffer();

	//Send message to iridium modem and wait for status update
	int status = isbd.sendReceiveSBDText(MessageString.c_str(), _rxBuffer, bufferSize);

	_IsTransmitting = false;

	//Run update values again/ expecting possible ring message which library doesn't handle/ This should hopefully clear the ring error
	UpdateStatus();

	if(_debugPrint)
	Serial.println("Irid Task - Sent Message Result:" + String(status));

	_MessagesWaiting = isbd.getWaitingMessageCount();

	//Handle status return
	if (status != ISBD_SUCCESS)
	{
		_CurrentState = SendStatus_Error;
		return SendFail;
	}else
	{
		_LastSendStamp = millis();
		_CurrentState = SendStatus_Sent;
	}

	

	//Flag message in buffer
	if (bufferSize != 0)
	{
		if(_debugPrint)
		Serial.println("+Irid Handler - Iridium Message receive found!");

		return SentAndReceived;
	}

	return SentNoReceive;
}

//***********************************************************************

//Take RX buffer and convert to String
boolean Iridium_HandlerClass::ParseReceiveToChar(char *ReturnString)
{

	if(_debugPrint)
	Serial.print("+Irid Handler - New Iridium Message Contents:");

	//Parse rx buffer to char array
	for (int i = 0; i <= RXBufferSize; i++)
	{
		if(_rxBuffer[i] == NULL || _rxBuffer[i] == _RXEndCharacter)
		{
			ReturnString[i] = NULL;
			break;
		}

		ReturnString[i] = _rxBuffer[i];
		
		if(_debugPrint)
		Serial.print(ReturnString[i]);
	}

	if(_debugPrint)
	Serial.println("\n End of message");
	

	return true;
}

//***********************************************************************

//Clear RX buffer after use
void Iridium_HandlerClass::ClearBuffer()
{
	for (int i = 0; i < RXBufferSize; i++)
	{
		_rxBuffer[i] = NULL;
	}
}

//***********************************************************************
//Time stamp check for ready to complete multiple transmits
boolean Iridium_HandlerClass::isReadyToTransmit()
{
	if(millis() - _LastSendStamp < 30000)
	return false;
	else
	return true;
}