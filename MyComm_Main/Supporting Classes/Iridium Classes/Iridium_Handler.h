//Iridium Handler Class
// Handles interactions with IridiumSBD library. Holds state values and general communications
//

#ifndef _IRIDIUM_HANDLER_h
#define _IRIDIUM_HANDLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "IridiumSBDLocal/IridiumSBD.h"

#ifndef _MyComm_Status_h
#define _MyComm_Status_h

//Status states for iridium
#define SendStaus_SentAndReceived 7
#define SendStatus_NewSend 1
#define SendStatus_Sending 2
#define SendStatus_Sent 3
#define IridStatus_Clear 0
#define SendStatus_Error -1
#define ReceiveStatus_NewMessage 4
#define ReceiveStatus_Checking 5
#define ReceiveStatus_NoWaiting 6
#define ReceiveStatus_Failed -2

#define Status_EmergencyMode 8
#define Emergency_Sent 81
#define Emergency_SentAndReceived 82



#endif

#define IridiumSerialPort Serial1 //Hardware serial port for iridium connection
#define IridiumSerialBaud 19200 //Baud rate for serial connection
#define PowerMode 1 //0 direct power, 1 USB power
#define IridSleepPin 23 //Pin toggle to put Iridium modem to sleep




//Status results from send attempt
#define SentAndReceived 1
#define SentNoReceive 0
#define SendFail -1

#define _RXEndCharacter '~' //End character used in RX messages to ensure end of message
#define RXBufferSize 300

#define IridMultiSendDelay 30000 //Delay time between multiple sends for iridium

class Iridium_HandlerClass
{
 protected:
 void ClearBuffer();

 public:

 boolean _IridiumSerialDebug = false;

 boolean Setup(); //Main setup

 boolean UpdateStatus(); //Update values with latest signal strength and number of messages waiting

 int SendMessageReceive(String MessageString);//Send string message. Return result of send attempt

 int getState() { return _CurrentState;}; //Return current state

 boolean ParseReceiveToChar(char *ReturnString);//Parse RX buffer to string

 int getMessages(); //Send status message and return available messages

 unsigned long getLastSendTime() {return _LastSendStamp;}; //Return timestamp of last send

 boolean isReadyToTransmit(); // Return is ready to transmit

 //Variables
 protected:
 boolean _IsTransmitting = false; // flag for iridium in transmit state
 boolean _debugPrint = true; //Debug serial print value
 int _CurrentState = 0; //Current iridium state
 uint8_t _rxBuffer[RXBufferSize]; //RX buffer store for received messages
 unsigned long _LastSendStamp = 0; //Timestamp for delaying multi sends

 public: 
 int _SignalStrength = -1;
 int _MessagesWaiting = -1;

};

#endif

