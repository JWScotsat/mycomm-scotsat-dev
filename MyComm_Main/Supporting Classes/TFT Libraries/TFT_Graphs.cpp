//
//
//

#include "TFT_Graphs.h"

void TFT_GraphsClass::setup(ILI9341_t3 *tft, int StartX, int StartY, int MaxX, int MaxY, int MinIn, int MaxIn)
{

	_tft = tft;

	_XPos = StartX;
	_YPos = StartY;
	_MaxX = MaxX;
	_MaxY = MaxY;
	_MinVal = MinIn;
	_MaxVal = MaxIn;

	

}

void TFT_GraphsClass::update(int NewVal)
{
	
	if(_StoredVal != NewVal)
	{

		if(NewVal < _StoredVal)
		_tft->fillRect(_XPos, _YPos, _MaxX - _XPos, _MaxY - _YPos, ILI9341_BLACK);

		_SpacingBars = map(NewVal, _MinVal, _MaxVal, 0, 10)+1;

		int lineSpacing = (_MaxX - _XPos)/10;

		int Newwidth = map(NewVal, _MinVal, _MaxVal, 0, _MaxX - _XPos);
				
		_tft->fillRect(_XPos, _YPos, Newwidth, _MaxY - _YPos, ILI9341_YELLOW);

		for (int i =0; i < _SpacingBars; i++)
		_tft->drawLine(_XPos + i*lineSpacing, _YPos, _XPos + i*lineSpacing, _MaxY, ILI9341_BLACK);

		_StoredVal = NewVal;
	}
}

void TFT_GraphsClass::drawBorder()
{
	_tft->drawLine(_XPos - 3, _YPos - 3, _XPos - 3, _MaxY+3, ILI9341_BLUE );
	_tft->drawLine(_MaxX + 1, _YPos - 3, _MaxX + 1, _MaxY+3, ILI9341_BLUE );
}

//TFT_GraphsClass TFT_Graphs;

