// TFT_Graphs.h

#ifndef _TFT_GRAPHS_h
#define _TFT_GRAPHS_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#ifndef ILI9341_BLACK
#include <ILI9341_t3.h>
#endif

class TFT_GraphsClass
{
	protected:
	ILI9341_t3 *_tft;

	int _XPos  = 0;
	int _YPos = 0;
	int _MaxX= 0;
	int _MaxY= 0;
	int _MaxVal= 0;
	int _MinVal= 0;
	int _StoredVal = 0;
	int _SpacingBars = 0;
	public:
	void setup(ILI9341_t3 *tft, int StartX, int StartY, int MaxX, int MaxY, int MinIn, int MaxIn);
	
	void update(int NewVal);
	void drawBorder();
};

//extern TFT_GraphsClass TFT_Graphs;

#endif

