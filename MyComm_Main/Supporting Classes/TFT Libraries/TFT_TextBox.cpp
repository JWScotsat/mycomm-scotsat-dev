//
//
//

#include "TFT_TextBox.h"

//***********************************************

void TFT_TextBoxClass::setup(ILI9341_t3 *tft)
{
	_tft = tft;
}

//***********************************************

//Set Text Box Size and Location in pixel term
void TFT_TextBoxClass::setLocation(int MinX, int MaxX, int MinY, int MaxY)
{
	_BoxSizeMinX = MinX;
	_BoxSizeMaxX = MaxX;
	_BoxSizeMinY = MinY;
	_BoxSizeMaxY = MaxY;

	_cursorMaxX = _BoxSizeMaxX - _CursorIndentX;
	_cursorMinX = _BoxSizeMinX + _CursorIndentX;
	
	_cursorMaxY = _BoxSizeMaxY - _CursorIndentY;
	_cursorMinY = _BoxSizeMinY + _CursorIndentY;

	_maxBoxLineY = _BoxSizeMaxY/_cursorsPosIncreaseY;
	_maxBoxLineX = _BoxSizeMaxX/_cursorsPosIncreaseX;

	_BoxcentreX = getCentreX();
	_BoxcentreY = getCentreY();
	_BoxHeight = getHeight();
	_BoxWidth = getWidth();
}

//***********************************************
// Set text box Font size inside the box. Changes settings for cursor movement to display text correctly
void TFT_TextBoxClass::setFontSize(int Size)
{
	_ReadTXTSize = Size;
	_cursorsPosIncreaseX = 6*_ReadTXTSize;
	_cursorsPosIncreaseY = _cursorsPosIncreaseX*1.5;
	_tft->setTextSize(_ReadTXTSize);
	_maxBoxLineY = _BoxSizeMaxY/_cursorsPosIncreaseY;
	_maxBoxLineX = _BoxSizeMaxX/_cursorsPosIncreaseX;
	_cursorLength = 7*_ReadTXTSize;
}

//***********************************************
// Set Colour
void TFT_TextBoxClass::setBackGroundColour(uint16_t Colour)
{
	_BackGroundColour = Colour;
}

void TFT_TextBoxClass::setTextColour(uint16_t Colour)
{
	_TextColour = Colour;
}

//***********************************************

//Clear text box with filled rectangle and reset cursor position for new text
void TFT_TextBoxClass::ResetBox()
{
	
	_tft->fillRoundRect(_BoxSizeMinX, _BoxSizeMinY, _BoxWidth, _BoxHeight, 5, _BackGroundColour);
	ResetCursor();

	ClearText();

	_charCount = 0;
	_lineCount = 0;
	_ScrollTextCount = 0;
}

//***********************************************

//Reset Cursor Position
void TFT_TextBoxClass::ResetCursor()
{
	//Set cursor to start position
	_tft->setCursor(_cursorMinX,_cursorMinY);
	_cursorPosX = _cursorMinX;
	_cursorPosY = _cursorMinY;
	

	_tft->setTextColor(_TextColour);
	_tft->setTextSize(_ReadTXTSize);
}

//***********************************************

//Print inside text box area. Return flag if out of bounds
boolean TFT_TextBoxClass::PrintText(char c)
{
	//Remove any cursor lines if blinking active
	if(_cursorBlinkEnable)
	RemoveCursor();

	//Backspace character input. Calls backspace function
	if(c == 0x08)
	{
		BackSpace();
		return true;
	}

	//Shift key input
	if(c == '~')
	return true;
	
	//Box edge roll over/wrap Plus new line scrolling
	if(_cursorPosX >= _cursorMaxX - _cursorsPosIncreaseX)
	{
		
		//Cursor position update.
		//Indent tracks the off-screen area due to text box & font spacing dimension differences
		_CursorEdgeIndentX = _cursorPosX - (_cursorMaxX - _cursorsPosIncreaseX);
		_cursorPosX = _cursorMinX;
		_cursorPosY += _cursorsPosIncreaseY;

		//New line scrolling
		if(_cursorPosY > (_cursorMaxY))
		ScrollTextBox();
	}

	//End of character storage for text box
	if(_cursorPosY > (_cursorMaxY) || _charCount >= CharArrayStore)
	return false;
	
	//Printing char and handle cursor spacing of font
	_tft->setTextColor(_TextColour);
	_tft->setTextSize(_ReadTXTSize);
	_tft->setCursor(_cursorPosX, _cursorPosY);
	_tft->print(c);

	//Update variables for tracking box contents
	_cursorPosX += _cursorsPosIncreaseX;
	_StoreTextArray[_charCount] = c;
	_StoreTextArray[_charCount+1] = NULL;
	_charCount++;
	_lineCount = _cursorPosY/_cursorsPosIncreaseY;

	return true;

}


//***********************************************

// Print inside text box area String. Return flag if out of bounds
boolean TFT_TextBoxClass::PrintText(String S)
{
	//Buffer String to char array
	char c[300];
	
	S.toCharArray(c,300,0);

	//Set text size for printing inside text box
	_tft->setTextSize(_ReadTXTSize);

	_tft->setTextColor(_TextColour);

	//Remove any cursor lines if blinking active
	if(_cursorBlinkEnable)
	RemoveCursor();


	//Print char array to screen
	for(int i = 0; i < S.length(); i++)
	{
		//Box edge roll over/wrap Plus new line scrolling
		if(_cursorPosX >= _cursorMaxX - _CursorIndentX)
		{
			_cursorPosX = _cursorMinX;
			_cursorPosY += _cursorsPosIncreaseY;

			if(_cursorPosY > (_cursorMaxY))
			ScrollTextBox();
		}

		//End of character storage for text box
		if(_cursorPosY > (_cursorMaxY) || _charCount > CharArrayStore)
		return false;

		//Printing char and handle cursor spacing of font
		_tft->setCursor(_cursorPosX, _cursorPosY);
		_tft->print(c[i]);

		//Update variables for tracking box contents
		_cursorPosX += _cursorsPosIncreaseX;		_StoreTextArray[_charCount] = c[i];		_charCount++;
		_lineCount = _cursorPosY/_cursorsPosIncreaseY;
		
	}

	_StoreTextArray[_charCount+1] = NULL;
	return true;
}

//************************************************

// Print inside text box area String. Return flag if out of bounds
boolean TFT_TextBoxClass::PrintText(String S, boolean Refresh)
{
	if(Refresh)ResetBox();
	//Buffer String to char array
	char c[300];
	
	S.toCharArray(c,300,0);

	//Set text size for printing inside text box
	_tft->setTextSize(_ReadTXTSize);

	_tft->setTextColor(_TextColour);

	//Remove any cursor lines if blinking active
	if(_cursorBlinkEnable)
	RemoveCursor();


	//Print char array to screen
	for(int i = 0; i < S.length(); i++)
	{
		//Box edge roll over/wrap Plus new line scrolling
		if(_cursorPosX >= _cursorMaxX - _CursorIndentX)
		{
			_cursorPosX = _cursorMinX;
			_cursorPosY += _cursorsPosIncreaseY;

			if(_cursorPosY > (_cursorMaxY))
			ScrollTextBox();
		}

		//End of character storage for text box
		if(_cursorPosY > (_cursorMaxY) || _charCount > CharArrayStore)
		return false;

		//Printing char and handle cursor spacing of font
		_tft->setCursor(_cursorPosX, _cursorPosY);
		_tft->print(c[i]);

		//Update variables for tracking box contents
		_cursorPosX += _cursorsPosIncreaseX;		_StoreTextArray[_charCount] = c[i];		_charCount++;
		_lineCount = _cursorPosY/_cursorsPosIncreaseY;
		
	}

	_StoreTextArray[_charCount+1] = NULL;
	return true;
}

//***********************************************

//Clear Storage Array
void TFT_TextBoxClass::ClearText()
{
	for(int i = 0; i < CharArrayStore; i++)
	_StoreTextArray[i] = NULL;
}

//***********************************************

//Return stored text in char array
int TFT_TextBoxClass::getText(char *ReturnArray)
{
	for (int i = 0; i <= _charCount; i++)
	{
// 		if(ReturnArray[i] == NULL)
// 		break;

		ReturnArray[i] = _StoreTextArray[i];
	}
	return _charCount;
}

//***********************************************
//Return text input length including NULL value
int TFT_TextBoxClass::getTextLength()
{
	return _charCount + 1;
}


//***********************************************

//Shift Text box display up one line, set cursor appropriately and re-write box
void TFT_TextBoxClass::ScrollTextBox()
{
	//Remove any cursor lines if blinking active
	if(_cursorBlinkEnable)
	RemoveCursor();

	//Keep track of number of line movements. This effects where in the Storage array to fill the rest of the box.
	_ScrollTextCount++;

	//Clear Text box
	_tft->fillRoundRect(_BoxSizeMinX, _BoxSizeMinY, _BoxWidth, _BoxHeight, 5, _BackGroundColour);
	
	//Shift cursor to start of text box
	_cursorPosX = _cursorMinX;
	_cursorPosY = _cursorMinY;

	//Serial.println("Max Box Line X:" + String(_maxBoxLineX));
	
	//Set text size for printing inside box
	_tft->setTextSize(_ReadTXTSize);

	//Start of Storage Array for printing data to box
	int i = _maxBoxLineX*_ScrollTextCount;

	// Print stored data to the text box. Should leave 1 line of free space for further text
	for (i ; i < _charCount ; i++)
	{
		//Box edge roll over/wrap
		if(_cursorPosX >= _cursorMaxX - _cursorsPosIncreaseX)
		{
			_cursorPosX = _cursorMinX;
			_cursorPosY += _cursorsPosIncreaseY;
		}

		//Text writing with cursor spacing to match text size.
		_tft->setCursor(_cursorPosX, _cursorPosY);
		_tft->print(_StoreTextArray[i]);
		_cursorPosX += _cursorsPosIncreaseX;
	}

}

//***********************************************

//BackSpace Implementation
void TFT_TextBoxClass::BackSpace()
{
	//Remove any cursor lines if blinking active
	if(_cursorBlinkEnable)
	RemoveCursor();

	//Check for being at start position - can't go any further back
	if(_charCount <= 0)
	return;

	//Backtrack cursor position in x
	_cursorPosX -= _cursorsPosIncreaseX;


	//Check for reverse roll over - going negative results in shifting up a line in Y and moving back from max X.
	if(_cursorPosX < _cursorMinX)
	{
		
		_cursorPosX = _cursorMaxX + _CursorEdgeIndentX - _cursorsPosIncreaseX*2;
		_cursorPosY -= _cursorsPosIncreaseY;

		if(_cursorPosY < _cursorMinY && _charCount > 2)
		{
			_cursorPosY += _cursorsPosIncreaseY;
			_cursorPosX = _cursorMinX;

			for(int i = 0; i < _charCount; i++)
			PrintText(_StoreTextArray[i]);

		}
	}

	//Use rectangle to fill text position of removed character.
	_tft->fillRect(_cursorPosX, _cursorPosY, _cursorsPosIncreaseX + _CursorIndentX, _cursorsPosIncreaseY, _BackGroundColour);

	//Remove character from storage array
	_StoreTextArray[_charCount] = NULL;

	//Update line and char count
	_lineCount = _cursorPosY/_cursorsPosIncreaseY;
	_charCount -= 1;

}

//***********************************************
void TFT_TextBoxClass::BlinkCursor()
{
	if(!_cursorBlinkEnable)
	return;

	if(millis() - _cursorBlinkStamp > _cursorBlinkRate)
	{
		//New line scrolling
		if(_cursorPosY > (_cursorMaxY))
		ScrollTextBox();
		

		if(_cursorBlinkState)
		DrawCursor();
		else
		RemoveCursor();

		_cursorBlinkState = !_cursorBlinkState;
		_cursorBlinkStamp = millis();
	}
}

//***********************************************
void TFT_TextBoxClass::DrawCursor()
{
	_tft->drawFastVLine(_cursorPosX + _cursorBlinkIndentX ,_cursorPosY, _cursorLength, _TextColour);
}

//***********************************************
void TFT_TextBoxClass::RemoveCursor()
{
	if(_cursorPosY < (_cursorMaxY))
	_tft->drawFastVLine(_cursorPosX + _cursorBlinkIndentX,_cursorPosY, _cursorLength, _BackGroundColour);
}

//***********************************************
int TFT_TextBoxClass::getCentreX()
{
	return (_BoxSizeMinX+(_BoxSizeMaxX - _BoxSizeMinX)/2);
}

//***********************************************
int TFT_TextBoxClass::getCentreY()
{
	return (_BoxSizeMinY+(_BoxSizeMaxY - _BoxSizeMinY)/2);
}

//***********************************************
int TFT_TextBoxClass::getHeight()
{
	return ((_BoxSizeMaxY - _BoxSizeMinY));
}

//***********************************************
int TFT_TextBoxClass::getWidth()
{
	return ((_BoxSizeMaxX - _BoxSizeMinX));
}


//***********************************************
//Return boolean value if touch position is inside the box
boolean TFT_TextBoxClass::isPressed(int x, int y)
{
	if (abs(x - _BoxcentreX) <= _BoxWidth/2 &&  abs(y - _BoxcentreY) <= _BoxHeight/2)
	{
		return true;
		
	}

	return false;
}


//***********************************************
TFT_TextBoxClass TFT_TextBox;

