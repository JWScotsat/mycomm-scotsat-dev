// TFT_TextBox.h

#ifndef _TFT_TEXTBOX_h
#define _TFT_TEXTBOX_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#ifndef ILI9341_BLACK
#include <ILI9341_t3.h>
#endif

#define CharArrayStore 300
class TFT_TextBoxClass
{
	protected:
	ILI9341_t3 *_tft;

	//*****************************

	//Colour Settings for Text and Background
	uint16_t _BackGroundColour = ILI9341_CYAN;
	uint16_t _TextColour = ILI9341_BLACK;

	//*****************************

	//Drawn Box Size Values
	int _BoxSizeMaxX = 240;
	int _BoxSizeMinX = 0;
	int _BoxSizeMaxY = 170;
	int _BoxSizeMinY = 20;
	int _BoxcentreX = 0;
	int _BoxcentreY = 0;
	int _BoxHeight = 0;
	int _BoxWidth = 0;

	//*****************************

	//Cursor Values for position and saving
	int _CursorIndentX = 5;
	int _CursorIndentY = 7;
	int _cursorMaxX = _BoxSizeMaxX - _CursorIndentX;
	int _cursorMinX = _BoxSizeMinX + _CursorIndentX;
	int _cursorMaxY = _BoxSizeMaxY + _CursorIndentY;
	int _cursorMinY = _BoxSizeMinY + _CursorIndentY;
	int _cursorsPosIncreaseX = 6*_ReadTXTSize;
	int _cursorsPosIncreaseY = _cursorsPosIncreaseX*1.5;
	int _cursorPosX = 0;
	int _cursorPosY = 0;
	int _maxBoxLineY = 0;
	int _maxBoxLineX = 0;
	int _CursorEdgeIndentX = 0;

	//*****************************
	//Font size
	uint8_t _ReadTXTSize = 2;
	//*****************************
	//Character Storage
	char _StoreTextArray[CharArrayStore];

	//*****************************
	//Scrolling Text box
	void ScrollTextBox();
	int _ScrollTextCount = 0;

	//*****************************

	//Cursor Blink functionality
	boolean _cursorBlinkState = false;
	unsigned long _cursorBlinkStamp = 0;
	void DrawCursor();
	void RemoveCursor();

	public:
	//Setup functions
	void setup(ILI9341_t3 *tft);
	void setLocation(int MinX, int MaxX, int MinY, int MaxY);
	void setFontSize(int Size);

	//Colour Setup
	void setBackGroundColour(uint16_t Colour);
	void setTextColour(uint16_t Colour);


	//*****************************

	//Reset/Clear box
	void ResetBox();
	void ResetCursor();
	void ClearText();

	//*****************************

	//Box Sizing Values
	int getCentreX();
	int getCentreY();
	int getHeight();
	int getWidth();

	//*****************************
	//Higher text box functionality
	void BackSpace();

	//*****************************
	//Return values for box
	int getText(char *ReturnArray);
	int getTextLength();

	//*****************************

	//Print text functionality
	boolean PrintText(char c);
	boolean PrintText(String S);
	boolean PrintText(String S, boolean Refresh);
	
	
	//*****************************

	//Cursor blinking functionality
	boolean _cursorBlinkEnable = false;
	unsigned long _cursorBlinkRate = 500;
	int _cursorLength = 15;
	int _cursorBlinkIndentX = 1;
	void BlinkCursor();

	//*****************************

	//Char count variables
	int _charCount = 0;
	int _lineCount = 0;

	//*****************************

	//Touch Support
	boolean isPressed(int x, int y);


};

extern TFT_TextBoxClass TFT_TextBox;

#endif

