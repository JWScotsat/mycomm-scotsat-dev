//
//
//

#include "TouchButton.h"

//Setup button location and size
void TouchButtonClass::setup(int CentreX , int CentreY, int Width, int Height, ILI9341_t3 *tft, const uint16_t *Image)
{

	_Height = Height;
	_Width = Width;
	_CentreX = CentreX;
	_CentreY = CentreY;
	_Image = Image;
	_tft = tft;
}

//Setup button location and size
void TouchButtonClass::setup(int CentreX , int CentreY,  int Width, int Height)
{

	_Height = Height;
	_Width = Width;
	_CentreX = CentreX;
	_CentreY = CentreY;
}
//************************************************************
boolean TouchButtonClass::isPressed(TSPoint p)
{

	if (abs(p.x - _CentreX) <= _Width/2 &&  abs(p.y - _CentreY) <= _Height/2)
	{
		return true;
		
	}
	
	return false;
}

void TouchButtonClass::Draw()
{
	int x = _CentreX - (_Width/2);
	int y = _CentreY - (_Height/2);

	_tft->writeRect(x, y, _Width, _Height, _Image);
}

TouchButtonClass TouchButton;

