// TouchButton.h

#ifndef _TOUCHBUTTON_h
#define _TOUCHBUTTON_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#ifndef ILI9341_BLACK
#include <ILI9341_t3.h>
#include <TouchScreen.h>
#endif


class TouchButtonClass
{
 protected:
 	int _Width;
 	int _Height;
 	int _CentreX;
 	int _CentreY;

	const uint16_t *_Image;
	ILI9341_t3 *_tft;
 public:
	void setup(int CentreX , int CentreY, int Width, int Height, ILI9341_t3 *tft, const uint16_t *Image);
	void setup(int CentreX , int CentreY, int Width, int Height);
	boolean isPressed(TSPoint p);
	void Draw();
};

extern TouchButtonClass TouchButton;

#endif

