//
//
//

#include "TouchKeyboard.h"

//General class setup for keypad. Requires TFT to print to
void TouchKeyboardClass::setup(ILI9341_t3 *tft)
{
	_tft = tft;

	_QBut.Letter = 'Q'; _QBut.LetterUpper = 'Q'; _QBut.LetterLower = 'q';
	_WBut.Letter = 'W'; _WBut.LetterUpper = 'W'; _WBut.LetterLower = 'w';
	_EBut.Letter = 'E'; _EBut.LetterUpper = 'E'; _EBut.LetterLower = 'e';
	_RBut.Letter = 'R'; _RBut.LetterUpper = 'R'; _RBut.LetterLower = 'r';
	_TBut.Letter = 'T'; _TBut.LetterUpper = 'T'; _TBut.LetterLower = 't';
	_YBut.Letter = 'Y'; _YBut.LetterUpper = 'Y'; _YBut.LetterLower = 'y';
	_UBut.Letter = 'U'; _UBut.LetterUpper = 'U'; _UBut.LetterLower = 'u';
	_IBut.Letter = 'I'; _IBut.LetterUpper = 'I'; _IBut.LetterLower = 'i';
	_OBut.Letter = 'O'; _OBut.LetterUpper = 'O'; _OBut.LetterLower = 'o';
	_PBut.Letter = 'P'; _PBut.LetterUpper = 'P'; _PBut.LetterLower = 'p';

	_ABut.Letter = 'A'; _ABut.LetterUpper = 'A'; _ABut.LetterLower = 'a';
	_SBut.Letter = 'S'; _SBut.LetterUpper = 'S'; _SBut.LetterLower = 's';
	_DBut.Letter = 'D'; _DBut.LetterUpper = 'D'; _DBut.LetterLower = 'd';
	_FBut.Letter = 'F'; _FBut.LetterUpper = 'F'; _FBut.LetterLower = 'f';
	_GBut.Letter = 'G'; _GBut.LetterUpper = 'G'; _GBut.LetterLower = 'g';
	_HBut.Letter = 'H';	_HBut.LetterUpper = 'H'; _HBut.LetterLower = 'h';
	_JBut.Letter = 'J'; _JBut.LetterUpper = 'J'; _JBut.LetterLower = 'j';
	_KBut.Letter = 'K'; _KBut.LetterUpper = 'K'; _KBut.LetterLower = 'k';
	_LBut.Letter = 'L'; _LBut.LetterUpper = 'L'; _LBut.LetterLower = 'l';
	_SpaceBut.Letter = ' '; _SpaceBut.LetterUpper = ' '; _SpaceBut.LetterLower = ' ';
	_SpaceBut2.Letter = ' '; _SpaceBut2.LetterUpper = ' '; _SpaceBut2.LetterLower = ' ';
	_ClearBut.Letter = 0x08; _ClearBut.LetterUpper = 0x08; _ClearBut.LetterLower = 0x08;
	
	_Shift.Letter = '~'; _Shift.LetterUpper = '~'; _Shift.LetterLower = '~';

		 _ZBut.Letter = 'Z'; _ZBut.LetterUpper = 'Z'; _ZBut.LetterLower = 'z';
		 _XBut.Letter = 'X'; _XBut.LetterUpper = 'X'; _XBut.LetterLower = 'x';
		 _CBut.Letter = 'C'; _CBut.LetterUpper = 'C'; _CBut.LetterLower = 'c';
		 _VBut.Letter = 'V'; _VBut.LetterUpper = 'V'; _VBut.LetterLower = 'v';
		 _BBut.Letter = 'B'; _BBut.LetterUpper = 'B'; _BBut.LetterLower = 'b';
		 _NBut.Letter = 'N'; _NBut.LetterUpper = 'N'; _NBut.LetterLower = 'n';
		 _MBut.Letter = 'M'; _MBut.LetterUpper = 'M'; _MBut.LetterLower = 'm';


}

//Draw the keypad on the screen inside specified area
void TouchKeyboardClass::DrawKeypad()
{

	for (int i = 0; i < 10; i++)
	{
		_tft->drawChar(TopX + i*KeySpaceX, TopY, getKeyValue(i), ILI9341_WHITE, ILI9341_RED, 3);

		
		KeyList[i]->CentreX = TopX + i*KeySpaceX + TextOffSet;
		KeyList[i]->CentreY = TopY + TextOffSet;
	}

	for (int i = 0; i < 10; i++)
	{
		_tft->drawChar(TopX + i*KeySpaceX, TopY + KeySpaceY , getKeyValue(i+10), ILI9341_WHITE, ILI9341_RED, 3);

		
		KeyList[i+10]->CentreX = TopX + i*KeySpaceX + TextOffSet;
		KeyList[i+10]->CentreY = TopY + TextOffSet  + KeySpaceY;
	}

	for (int i = 0; i < 10; i++)
	{
		_tft->drawChar(TopX + i*KeySpaceX, TopY + KeySpaceY*2 , getKeyValue(i+20), ILI9341_WHITE, ILI9341_RED, 3);

		
		KeyList[i+20]->CentreX = TopX + i*KeySpaceX + TextOffSet;
		KeyList[i+20]->CentreY = TopY + TextOffSet  + KeySpaceY*2;
	}

	//_tft->drawChar(TopX + 2*KeySpaceX, TopY + 2*KeySpaceY , KeyList[20]->Letter, ILI9341_WHITE, ILI9341_RED, 3);

}



//Return keypressed letter
char TouchKeyboardClass::GetKeyPress(TSPoint p)
{
	for(int i = 0; i < _KeybutSize; i++)
	{
		if (abs(p.x - KeyList[i]->CentreX) <= KeyList[i]->KeyBoxSize/2 &&  abs(p.y - KeyList[i]->CentreY) <= KeyList[i]->KeyBoxSize/2)
		{

			char Letter = getKeyValue(i);

			Serial.print(Letter);
			
			if(Letter == _Shift.Letter)
			{
				if(_keypadMode == 0)
					_keypadMode = 1;
				else
					_keypadMode = 0;
					
				DrawKeypad(); 
			}

			return Letter;
			
			

		}
	}
	
	return NULL;
}

//Returns key value for keyindex based on current keypad mode (Upper/Lower etc)
char TouchKeyboardClass::getKeyValue(int KeyIndex)
{

	switch (_keypadMode)
	{
		case 0: return KeyList[KeyIndex]->LetterLower;
		case 1: return KeyList[KeyIndex]->LetterUpper;
		default: return KeyList[KeyIndex]->Letter;
	}
}

//************************************************************

TouchKeyboardClass TouchKeyboard;

