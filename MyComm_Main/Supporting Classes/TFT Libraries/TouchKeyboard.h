// TouchKeyboard.h

#ifndef _TOUCHKEYBOARD_h
#define _TOUCHKEYBOARD_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#ifndef ILI9341_BLACK
#include <ILI9341_t3.h>
#include <TouchScreen.h>
#endif

struct Keybut{
	char Letter;
	char LetterLower;
	char LetterUpper;
	int LetColour = ILI9341_WHITE;
	int BkgColour = ILI9341_BLACK;
	int KeyBoxSize = 23;
	int CentreX;
	int CentreY;
};






class TouchKeyboardClass
{
	protected:
	ILI9341_t3 *_tft;

	int TopY = 200;
	int SecY = TopY + KeySpaceY;

	int TopX = 8;
	int KeySpaceX = 23;
	int KeySpaceY = 40;

	Keybut _QBut;
	Keybut _WBut;
	Keybut _EBut;
	Keybut _RBut;
	Keybut _TBut;
	Keybut _YBut;
	Keybut _UBut;
	Keybut _IBut;
	Keybut _OBut;
	Keybut _PBut;
	Keybut _ABut;
	Keybut _SBut;
	Keybut _DBut;
	Keybut _FBut;
	Keybut _GBut;
	Keybut _HBut;
	Keybut _JBut;
	Keybut _KBut;
	Keybut _LBut;

	Keybut _ZBut;
	Keybut _XBut;
	Keybut _CBut;
	Keybut _VBut;
	Keybut _BBut;
	Keybut _NBut;
	Keybut _MBut;

	Keybut _SpaceBut;
	Keybut _SpaceBut2;
	Keybut _ClearBut;
	Keybut _Shift;

	int _KeybutSize = 30;

	Keybut *KeyList[30] = {&_QBut, &_WBut, &_EBut, &_RBut, &_TBut, &_YBut, &_UBut, &_IBut, &_OBut, &_PBut,
	&_ABut, &_SBut, &_DBut, &_FBut, &_GBut, &_HBut, &_JBut, &_KBut, &_LBut,&_ClearBut, 
	&_Shift, &_ZBut, &_XBut, &_CBut,&_SpaceBut ,&_SpaceBut2, &_VBut, &_BBut, &_NBut, &_MBut};
	 
	int TextOffSet = 10;

	public:
	void setup(ILI9341_t3 *tft);



	void DrawKeypad();
	char getKeyValue(int KeyIndex);
	int _keypadMode = 0;

	char GetKeyPress(TSPoint p);
};

extern TouchKeyboardClass TouchKeyboard;

#endif

