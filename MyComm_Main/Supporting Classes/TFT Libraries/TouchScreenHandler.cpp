//
//
//

#include "TouchScreenHandler.h"

void TouchScreenHandlerClass::setup(ILI9341_t3 *tft)
{
	_tft = tft;

}
//************************************************************
TSPoint TouchScreenHandlerClass::getTouchPoint()
{
	// Size of the color selection boxes and the paintbrush size
	const int PENRADIUS = 3;

	if ((_p.y+PENRADIUS) < _tftHeight && debugShowPointEnable) {
		_tft->fillCircle(_p.x, _p.y, PENRADIUS, ILI9341_YELLOW);
	}

	return _p;
}
//************************************************************

boolean TouchScreenHandlerClass::pointDebounce()
{
	// Retrieve a point
	
	_p = _ts.getPoint();

	static unsigned long BounceAge = millis();
	const unsigned long BounceDelay = 250;
	const int BouncePosDiff = 50;
	static TSPoint prevPoint;
	// we have some minimum pressure we consider 'valid'
	// pressure of 0 means no pressing!

	if (_p.z < MINPRESSURE || _p.z > MAXPRESSURE) {
		return false;
	}else
	if(millis()- BounceAge > BounceDelay || abs(prevPoint.x - _p.x) > BouncePosDiff || abs(prevPoint.y - _p.y) > BouncePosDiff)
	{
		BounceAge = millis();
		prevPoint = _p;

		//
		// 		if(debugPrintCoords)
		// 		{
		// 			Serial.print("\nX = "); Serial.print(p.x);
		// 			Serial.print("\tY = "); Serial.print(p.y);
		// 			Serial.print("\tPressure = "); Serial.println(p.z);
		// 		}
		
		
		
		
		// Scale from ~0->1000 to tft.width using the calibration #'s
		_p.x = map(_p.x, TS_MINX, TS_MAXX, 0, _tftWidth);
		_p.y = map(_p.y, TS_MINY, TS_MAXY, 0, _tftHeight);

		return true;
	}

	return false;
}

//************************************************************

// Return current touch state
boolean TouchScreenHandlerClass::isTouching()
{
	_p = _ts.getPoint();

	if (_p.z < MINPRESSURE || _p.z > MAXPRESSURE)
	return false;

	return true;

}

TouchScreenHandlerClass TouchScreenHandler;

