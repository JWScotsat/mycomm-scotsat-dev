// TouchScreenHandler.h

#ifndef _TOUCHSCREENHANDLER_h
#define _TOUCHSCREENHANDLER_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "TouchScreen.h"

// These are the four touchscreen analog pins
#define YP A2  // must be an analog pin, use "An" notation!
#define XM A3  // must be an analog pin, use "An" notation!
#define YM 5   // can be a digital pin
#define XP 4   // can be a digital pin

// This is calibration data for the raw touch data to the screen coordinates
#define TS_MINX 120//150
#define TS_MINY 100//120
#define TS_MAXX 910//920
#define TS_MAXY 915//940

#define MINPRESSURE 10
#define MAXPRESSURE 1000

#ifndef ILI9341_BLACK
#include <ILI9341_t3.h>
#endif

# define debugShowPointEnable true

class TouchScreenHandlerClass
{
	protected:
	ILI9341_t3 *_tft;
	int _tftWidth = 240;
	int _tftHeight = 320;
	public:
	void setup(ILI9341_t3 *tft);
	boolean isTouching();
	boolean pointDebounce();

	TSPoint getTouchPoint();

	TSPoint _p;
	// For better pressure precision, we need to know the resistance
	// between X+ and X- Use any multimeter to read it
	// For the one we're using, its 300 ohms across the X plate
	TouchScreen _ts = TouchScreen(XP, YP, XM, YM, 295);
	
	boolean isReleased = true;
};

extern TouchScreenHandlerClass TouchScreenHandler;

#endif

