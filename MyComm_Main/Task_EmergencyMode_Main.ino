//Emergency Mode Task
// Sets MyComm into emergency mode when emergency button is head for 5 seconds
// Low priority task as main functionality is handled by other tasks
static void vTaskEmergencyMode(void *pvParameters)
{
	const int EmergencyPin = 2;

	pinMode(EmergencyPin,INPUT_PULLUP);

	boolean EmergencyActivated = false;

	int HoldCount = 0;
	while(1)
	{
		if(!digitalRead(EmergencyPin)) //Pull up so reverse read (Low is pressed)
		HoldCount++;
		else
		HoldCount = 0;

		//Set Global Status to Emergency on button held for 10 counts = 5 seconds
		if(HoldCount > 10 && !EmergencyActivated)
		{
			Serial.println("Emergency Mode Activated!");
			_GlobalMyCommStatus = Status_EmergencyMode;
			EmergencyActivated = true;

		}

		//Sleep task 501ms
		vTaskDelay((501 * configTICK_RATE_HZ) / 1000L);

	}
}
