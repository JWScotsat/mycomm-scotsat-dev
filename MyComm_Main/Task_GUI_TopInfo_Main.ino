//GUI Elements for top info bar - similar to smartphone notification bar

//Main FreeRTOS task
// Rests for 1 second between updates
static void vTopInfoGUITask(void *pvParameters)
{
	GUI_TopInfoBarClass TopInfoBar;

	TopInfoBar.setup(tft);

	while(1)
	{
		// Sleep for 1000 milliseconds.
		vTaskDelay((1000L * configTICK_RATE_HZ) / 1000L);
		UpdateTopInfo(&TopInfoBar);
	}
}

//**************************************************************************

#define SerialPrintTrigger 5 //Counter increases every second so trigger's in seconds

//Redraws top info bar with latest status elements
void UpdateTopInfo(GUI_TopInfoBarClass *TopInfoBar)
{

	static unsigned long SerialPrintCounter = 0;

	//Iridium stats
	int signalQuality = _GlobalSignalQuality;
	int messagesWaiting = _GlobalMessageWaiting;


	//Colour controls top info bar based on signal strength availible from Iridium
	TopInfoBar->UpdateStatusColour(signalQuality);

	//Update Text String for Top Info Box
	TopInfoBar->UpdateText(signalQuality,messagesWaiting, _GlobalMyCommStatus, CodeVersion,_GlobalQuedMessage, _GlobalMessageFileQueIndex);

	noInterrupts();
	//Write latest box details to screen
	TopInfoBar->UpdateScreen();
	interrupts();

	//Serial printout of status
	if(SerialPrintCounter >= SerialPrintTrigger)
	{
		Serial.println("Status: " + TopInfoBar->getContents());
		SerialPrintCounter = 0;
	}


	SerialPrintCounter++;

	//Sensor internal temp read. Added here until secondary task for sensor reading is created
	_GlobalInternalTemp = InternalTempRead();
	
	
}


